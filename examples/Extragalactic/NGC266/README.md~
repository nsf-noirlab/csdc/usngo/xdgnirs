![NGC266_image](/examples/Galactic/NGC266/NGC266_image.png "NGC266_image.png")
# **NGC266**

In this example, we will reduce the raw data for NGC266 and use the 'wave_soln_linear' to toggle the reduced spectrum's wavelength solution between 'LINEAR' and 'MULTISPE'. Once all of the data is downloaded and placed into the working directory, you can enter the following command into the Virtual Machine's command prompt to obtain the reduced spectra with the default header keyword CTYPE='MULTISPE':

./../XDGNIRS/XDpiped.csh inputfiles.lst

![Data_Sheet_MULTISPE](NGC266_data_sheet_multispe.png "Data Sheet with CTYPE='MULTISPE'") ![Multisp_header](Multisp_header.png "Header of the reduced spectrum with CTYPE='MULTISPE'")

If you would prefer for the output spectrum to have CTYPE='Linear', you can use the pipeline's wave_soln_linear option as so:

./../XDGNIRS/XDpiped.csh inputfiles.lst wave_soln_linear='yes'

![Data_Sheet_LINEAR](NGC266_data_sheet_Linear.png "Data Sheet with CTYPE='LINEAR'") ![Linear_header](Linear_header.png "Header of the reduced spectrum with CTYPE='LINEAR'")

Despite the fact that the CRTYPEs are different, the final spectra are ultimately unchanged. When you compare both data sheet's spectra together, you can see that this option soley changed the header, and not the spectrum itself. 


# _Image Credit:_
This example has made use of an image from the "Aladin sky atlas" developed at CDS, Strasbourg Observatory, France

→ 2000A&AS..143...33B (Aladin Desktop), 2014ASPC..485..277B (Aladin Lite v2), and 2022ASPC..532....7B (Aladin Lite v3).