![NGC3079_image](/Images/NGC3079_image.png "NGC3079_image.png")
# __**NGC3079**__

NGC3079 was observed on December 1st, 2012 ([GN-2012A-Q-23](https://archive.gemini.edu/searchform/cols=CTOWBEQ/NotFail/not_site_monitoring/notengineering/20120123/GN-2012A-Q-23)).

![NGC3079](/Images/NGC3079_data_sheet.png "NGC3079 datasheet.png") *NGC3079 datasheet*

![NGC3079_pub](/Images/NGC3079_hr.jpg "NGC3079_hr.jpg") *NGC3079 published spectrum*

# _Image Credit:_
This example has made use of an image from the "Aladin sky atlas" developed at CDS, Strasbourg Observatory, France

→ 2000A&AS..143...33B (Aladin Desktop), 2014ASPC..485..277B (Aladin Lite v2), and 2022ASPC..532....7B (Aladin Lite v3).