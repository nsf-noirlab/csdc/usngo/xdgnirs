To access the data files used in this example, go to the [Gemini Observatory Archive](https://archive.gemini.edu/searchform/cols=CTOWBEQ/NotFail/not_site_monitoring/notengineering/20120123/GN-2012A-Q-23) and download the files listed in inputfiles.lst.

Alternatively, you can wget with the wget_list.txt file provided in this directory. Make sure that wget is installed in your computer and run the following command in your terminal:
## wget -i wget_list.txt

This command should download all of the required files and place them in your current working directory. 
