![Galaxy_banner](/Images/Galaxy_banner.jpg "Galaxy_banner.jpg") *Credit:International Gemini Observatory/NSF’s NOIRLab/AURA/Kwon o chul*

This directory contains thirteen galactic science cases used in the science verification process. The sources included are:

- [1H1934-063](https://gitlab.com/nsf-noirlab/csdc/usngo/xdgnirs/-/tree/main/examples/Extragalactic/1H1934-063)
- [NGC1961](https://gitlab.com/nsf-noirlab/csdc/usngo/xdgnirs/-/tree/main/examples/Extragalactic/NGC1961)
- [NGC2273](https://gitlab.com/nsf-noirlab/csdc/usngo/xdgnirs/-/tree/main/examples/Extragalactic/NGC2273)
- [NGC266](https://gitlab.com/nsf-noirlab/csdc/usngo/xdgnirs/-/tree/main/examples/Extragalactic/NGC266)
- [NGC3031 (M81)](https://gitlab.com/nsf-noirlab/csdc/usngo/xdgnirs/-/tree/main/examples/Extragalactic/NGC3031)
- [NGC3079](https://gitlab.com/nsf-noirlab/csdc/usngo/xdgnirs/-/tree/main/examples/Extragalactic/NGC3079)
- [NGC3190](https://gitlab.com/nsf-noirlab/csdc/usngo/xdgnirs/-/tree/main/examples/Extragalactic/NGC3190)
- [NGC4388](https://gitlab.com/nsf-noirlab/csdc/usngo/xdgnirs/-/tree/main/examples/Extragalactic/NGC4388)
- [NGC4395](https://gitlab.com/nsf-noirlab/csdc/usngo/xdgnirs/-/tree/main/examples/Extragalactic/NGC4395)
- [NGC4450](https://gitlab.com/nsf-noirlab/csdc/usngo/xdgnirs/-/tree/main/examples/Extragalactic/NGC4450)
- [NGC4579](https://gitlab.com/nsf-noirlab/csdc/usngo/xdgnirs/-/tree/main/examples/Extragalactic/NGC4579)
- [NGC4736](https://gitlab.com/nsf-noirlab/csdc/usngo/xdgnirs/-/tree/main/examples/Extragalactic/NGC4736)
- [NGC4762](https://gitlab.com/nsf-noirlab/csdc/usngo/xdgnirs/-/tree/main/examples/Extragalactic/NGC4762)

![Table_of_Galaxy_Properties](/Images/Galaxy_Table.png "Galaxy_Table.png") *\
Table of redshifts and radial velocities for the above sources. All of the values included in this table come from the [SIMBAD Astronomical Database](http://simbad.cds.unistra.fr/simbad/)*

Twelve of the above galaxies are presented in [R. Mason et al. 2015](https://gitlab.com/nsf-noirlab/csdc/usngo/xdgnirs/-/tree/main/examples/Galactic/Mason_2015.pdf). Each example contains the data sheet produced when running the GNIRS XD pipeline as well as the spectrum that was publsihed in this paper (with NGC 4762 being the lone exception). 