![NGC3031_image](/Images/NGC3031_image.png "NGC3031_image.png")
# **NGC3031**

NGC3031 (also known as M81) is the galaxy that is examined in XDGNIRS_v226.pdf.

![NGC3031](/Images/NGC3031_data_sheet.png "NGC3031_data_sheet.png") *NGC3031_datasheet obtained using the provied inputfiles.lst*

![NGC3031_manual](/Images/NGC3031_manual.png "NGC3031_manual.png") *Data sheet included in XD GNIRS Manual*

# _Image Credit:_
This example has made use of an image from the "Aladin sky atlas" developed at CDS, Strasbourg Observatory, France

→ 2000A&AS..143...33B (Aladin Desktop), 2014ASPC..485..277B (Aladin Lite v2), and 2022ASPC..532....7B (Aladin Lite v3).