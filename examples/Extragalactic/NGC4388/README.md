![NGC4388_image](/Images/NGC4388_image.png "NGC4388_image.png")
# _**NGC4388**_

![NGC4388](/Images/NGC4388_data_sheet.png "NGC4388 datasheet.png") *NGC4388 datasheet*

![NGC4388_pub](/Images/NGC4388_hr.jpg "NGC4388_hr.jpg") *NGC4388 published spectrum*

# _Image Credit:_
This example has made use of an image from the "Aladin sky atlas" developed at CDS, Strasbourg Observatory, France

→ 2000A&AS..143...33B (Aladin Desktop), 2014ASPC..485..277B (Aladin Lite v2), and 2022ASPC..532....7B (Aladin Lite v3).