![NGC3190_image](/Images/NGC3190_image.png "NGC3190_image.png")
# __**NGC3190**__

NGC3190 was observed on November 27th, 2011 ([GN-2011B-Q-111](https://archive.gemini.edu/searchform/cols=CTOWBEQ/NotFail/not_site_monitoring/notengineering/20111127)).

![NGC3190](/Images/NGC3190_data_sheet.png "NGC3190 datasheet.png") *NGC3190 datasheet*

![NGC3190_pub](/Images/NGC3190_hr.jpg "NGC3190_hr.jpg") *NGC3190 published spectrum*


# _Image Credit:_
This example has made use of an image from the "Aladin sky atlas" developed at CDS, Strasbourg Observatory, France

→ 2000A&AS..143...33B (Aladin Desktop), 2014ASPC..485..277B (Aladin Lite v2), and 2022ASPC..532....7B (Aladin Lite v3).