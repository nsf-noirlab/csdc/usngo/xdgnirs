![NGC4762_image](/Images/NGC4762_image.png "NGC4762_image.png")
# **NGC4762**
## **GN-2012A-Q-120**
In this example, we will reduce the spectrum like in the other examples, but here, we will correct the wavelegnths for redshift. The pipeline's default is to present the spectrum in the observation frame, but there is a way to transform the spectrum into the rest wavelength frame.

Once all of the appropriate data is downloaded and placed into the working directry, you can enter the following command into the Virtual Machine's command prompt to obtain the reduced spectrum in the observation frame:

./../XDGNIRS/XDpiped.csh inputfiles.lst despike_tgt='yes' despike_std='yes'

The two despike statements are necessary for this step becasue data taken before June 2012 contain spikes due to a previous len's radioactive coating in the SB camera. When the pipeline is done running, you should have a reduced spectrum that resembles the following:

![NGC4762_obs_frame](/Images/NGC4762_obs_frame.png "NGC4762's spectrum in the observation frame.") *\
NGC4762's spectrum in the observation frame.*

We can compare this reduced product to a published spectrum for this source and see that the spectra match rather well, specifically the four absorption features that start around 2.30 $\mu m$ up until about 2.40 $\mu m$.

![NGC4762_published_spectrum](/Images/NGC4762_published_spectrum.png "NGC4762's published spectrum.") *\
Published spectrum for NGC4762 from figure 1 of [Davor Krajnovic et al. 2018](https://academic.oup.com/mnras/article/477/3/3030/4952019).*

Now that we know what the spectrum looks like in the observation frame and how it compares to a published spectrum, lets shift the spectrum into the rest frame. To do so, we will use a slightly different version of the command that we used previously. Enter the following command into the command prompt:

./../XDGNIRS/XDpiped.csh inputfiles.lst despike_tgt='yes' despike_std='yes' shift_to_rest='yes'

The 'shift_to_rest' option will query the SIMBAD Astronomical Database using the name of the source that is provided in the image header. If the query is able to find an entry whose name matches the source's, it will report the redshift (if one is available) back to the pipeline. Even if SIMBAD doesn't have an explicit redshift value for the source, as long as a radial velocity value is present, the pipeline will be able to calculate the source's redshift.

![NGC4762_obs_and_rest_frame](/Images/NGC4762_obs_and_rest_frames.png "NGC4762's spectrum in the observation and rest frames.") *\
NGC4762's spectrum in the observation and rest frames.*

When you examine the two spectra, you may wonder if the 'shift_to_rest' option worked since both spectra are sitting right on top of each other. In reality, the spectrum did shift, but this source has a very small redshift of z=0.00287 which corresponds to a tiny shift. For example, if you were to take the the absorption feature at $2.30 \mu m$ and shift it into the rest frame, it would only move by $0.01\mu m$, which explains why the above plot may be slightly underwhelming. 

Now that we have seen how to automatically shift the spectrum into the rest frame, lets try manually providing the pipeline with a redshift value. This method can be used when SIMBAD does not have a redshift for your target in their database, but it is known by the user.

To do this, you will need to create a new text file and enter the redshift there. The pipeline will detect the presence of this file when the 'shift_to_rest' is called and the provided value will be used to shift the final spectrum into the rest frame. Still using the same NGC4762 data, lets try running the pipeline again by manually providing the galaxy's actual redshift and one more time for some arbitrary redshift value. NGC4762's redshift is z=0.00287, as provided on the table from the [Galactic Directory](https://gitlab.com/nsf-noirlab/csdc/usngo/xdgnirs/-/tree/main/examples/Galactic).

First, lets create an new txt file with the redshift value. For this example, lets set z=15.

![redshift_txt](/Images/redshift_txt.png "The redshift file that we provide to the pipeline should only contain the source's redshift.") *\
Redshift text file*

The next step depends on whether or not you have already ran the pipeline on the source's data. If you have, the pipeline creates a directory called 'INTERMEDIATE' and if you haven't ran the pipeline yet, you will need to create the directory using the command 'mkdir INTERMEDIATE'. Note: this directory name is case sensitive. Once the directory is established, you will need to add the text file with the source's redshift into the INTERMEDIATE directory under the name 'redshift.txt'.

Finally, you can run the following command in your terminal:
./../XDGNIRS/XDpiped.csh inputfiles.lst despike_tgt='yes' despike_std='yes' shift_to_rest='yes'

Once the pipeline is done running, there will be a directory called PRODUCTS that will contain two fits files that contain the reduced spectra. The fits file that only has the soruce's name contains the spectrum in the observation frame, and the other fits file with the suffix '_rest.fits' contains the spectrum in the rest frame. 


![NGC4762_obs_and_rest_frame](/Images/NGC4762_obs_and_rest_frame.png "NGC4762's spectra in the rest and observation frames.") *\
NGC4762's spectra in the observation and rest frames.*


Warning!
The ability to manually enter a source's redshift may come in handy when deadling with new sources but can leave room for problems. Make sure to double check the redshift value before providing the pipeline with the text file.


# _Image Credit:_
1. This example has made use of an image from the "Aladin sky atlas" developed at CDS, Strasbourg Observatory, France

   → 2000A&AS..143...33B (Aladin Desktop), 2014ASPC..485..277B (Aladin Lite v2), and 2022ASPC..532....7B (Aladin Lite v3).

2. Krajnovic, D., Cappellari, M., McDermid, R. M., et al. 2018, MNRAS, 477, ´
3030