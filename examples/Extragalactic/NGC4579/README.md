![NGC4579_image](/Images/NGC4579_image.png "NGC4579_image.png")
# _**NGC4579**_

![NGC4579](/Images/NGC4579_data_sheet.png "NGC4579 datasheet.png") *NGC4579 datasheet*

![NGC4579_pub](/Images/NGC4579_hr.png "NGC4579_hr.png") *NGC4579 published spectrum*

# _Image Credit:_
This example has made use of an image from the "Aladin sky atlas" developed at CDS, Strasbourg Observatory, France

→ 2000A&AS..143...33B (Aladin Desktop), 2014ASPC..485..277B (Aladin Lite v2), and 2022ASPC..532....7B (Aladin Lite v3).