![NGC4395_image](/Images/NGC4395_image.png "NGC4395_image.png")
# _**NGC4395**_

![NGC4395](/Images/NGC4395_data_sheet.png "NGC4395 datasheet.png") *NGC4395 datasheet*

![NGC4395_pub](/Images/NGC4395_hr.jpg "NGC4395_hr.jpg") *NGC4395 published spectrum*

# _Image Credit:_
This example has made use of an image from the "Aladin sky atlas" developed at CDS, Strasbourg Observatory, France

→ 2000A&AS..143...33B (Aladin Desktop), 2014ASPC..485..277B (Aladin Lite v2), and 2022ASPC..532....7B (Aladin Lite v3).