![NGC4450_image](/Images/NGC4450_image.png "NGC4450_image.png")
# _**NGC4450**_

![NGC4450](/Images/NGC4450_data_sheet.png "NGC4450 datasheet.png") *NGC4450 datasheet*

![NGC4450_pub](/Images/NGC4450_hr.jpg "NGC4450_hr.jpg") *NGC4450 published spectrum*

# _Image Credit:_
This example has made use of an image from the "Aladin sky atlas" developed at CDS, Strasbourg Observatory, France

→ 2000A&AS..143...33B (Aladin Desktop), 2014ASPC..485..277B (Aladin Lite v2), and 2022ASPC..532....7B (Aladin Lite v3).