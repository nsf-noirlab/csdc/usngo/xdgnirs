This directory contains fifteen science cases that were used in the science verification process. The cases are broken up into two stellar and thirteen extragalactic examples. 

Stellar
- [Wise0410](https://gitlab.com/nsf-noirlab/csdc/usngo/xdgnirs/-/tree/main/examples/Stellar/wise0410)
- [HH212B](https://gitlab.com/nsf-noirlab/csdc/usngo/xdgnirs/-/tree/main/examples/Stellar/HH212B)

Extragalactic
- [1H1934-063](https://gitlab.com/nsf-noirlab/csdc/usngo/xdgnirs/-/tree/main/examples/Extragalactic/1H1934-063)
- [NGC1961](https://gitlab.com/nsf-noirlab/csdc/usngo/xdgnirs/-/tree/main/examples/Extragalactic/NGC1961)
- [NGC2273](https://gitlab.com/nsf-noirlab/csdc/usngo/xdgnirs/-/tree/main/examples/Extragalactic/NGC2273)
- [NGC266](https://gitlab.com/nsf-noirlab/csdc/usngo/xdgnirs/-/tree/main/examples/Extragalactic/NGC266)
- [NGC3031](https://gitlab.com/nsf-noirlab/csdc/usngo/xdgnirs/-/tree/main/examples/Extragalactic/NGC3031)
- [NGC3079](https://gitlab.com/nsf-noirlab/csdc/usngo/xdgnirs/-/tree/main/examples/Extragalactic/NGC3079)
- [NGC3190](https://gitlab.com/nsf-noirlab/csdc/usngo/xdgnirs/-/tree/main/examples/Extragalactic/NGC3190)
- [NGC4388](https://gitlab.com/nsf-noirlab/csdc/usngo/xdgnirs/-/tree/main/examples/Extragalactic/NGC4388)
- [NGC4395](https://gitlab.com/nsf-noirlab/csdc/usngo/xdgnirs/-/tree/main/examples/Extragalactic/NGC4395)
- [NGC4450](https://gitlab.com/nsf-noirlab/csdc/usngo/xdgnirs/-/tree/main/examples/Extragalactic/NGC4450)
- [NGC4579](https://gitlab.com/nsf-noirlab/csdc/usngo/xdgnirs/-/tree/main/examples/Extragalactic/NGC4579)
- [NGC4736](https://gitlab.com/nsf-noirlab/csdc/usngo/xdgnirs/-/tree/main/examples/Extragalactic/NGC4736)
- [NGC4762](https://gitlab.com/nsf-noirlab/csdc/usngo/xdgnirs/-/tree/main/examples/Extragalactic/NGC4762)
