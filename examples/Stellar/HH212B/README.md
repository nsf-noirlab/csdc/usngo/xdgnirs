# **HH212B**

The following two images are the reduced spectrum for HH 212 B a spectrum published by Reipurth et al. 2019. Both spectra have a gap between 18,000 and 19,000 Angstroms. This is because there is known strong atmospheric absorption between these wavelengths. The shape of both spectra are identical, while ours appears noiser than the publsihed version. This is because the published spectrum has been normalized while ours has not.

![HH212B_reduced](/Images/HH212B_Reduced_Spectrum.png "HH212B_Reduced_Spectrum.png") *\
Reduced spectrum for HH212B*

![HH212B_published](/Images/HH212B_Published_Spectrum.png "HH212B_Published_Spectrum.png") *\
Published spectrum for HH212B*

# Image Credit:
----
Reipurth et al. 2019