import numpy as np
import matplotlib.pyplot as plt
from astropy.table import Table

file = 'HH212B.txt'
cat = Table.read(file,format='ascii')

wave = cat['col1']
flux = cat['col2']

#match = (cat['col1'] <= 18000) & (cat['col1'] >=19000)
left  = 18250
right = 19000
top = cat['col1'] >= right
bot = cat['col1'] <= left


#plt.scatter(wave,flux,s=1,ls='-')
#plt.plot(wave,flux,markersize=1,ls='-',linewidth=.5)
plt.plot(wave[top],flux[top],markersize=1,ls='-',linewidth=.5,color='black')
plt.plot(wave[bot],flux[bot],markersize=1,ls='-',linewidth=.5,color='black')
plt.vlines(left,1*10**(-18),4*10**(-17),ls=':',color='black',alpha=0.5)
plt.vlines(right,1*10**(-18),4*10**(-17),ls=':',color='black',alpha=0.5)

plt.xlim(15000,25000)
plt.ylim(1*10**(-18),4*10**(-17))
plt.xlabel('Wavelength',fontsize=18,fontweight='bold')
plt.ylabel('Flux',fontsize=18,fontweight='bold')
plt.text(15100,3.25*10**(-17),"HH 212 IRS-B",fontsize=16)#,fontweight='bold')
plt.show()
