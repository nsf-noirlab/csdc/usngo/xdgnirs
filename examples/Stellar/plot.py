import numpy as np
import matplotlib.pyplot as plt
from astropy.table import Table

file = 'HH212B.txt'
cat = Table.read(file,format='ascii')

wave = cat['col1']
flux = cat['col2']

plt.scatter(wave[0],flux[0],s=1,c='orange',zorder=100)

plt.xlim(1.5,2.5)
plt.ylim(5*10**(-18),4*10**(-17))
plt.show()
