<table border="0">
<tr>
  <td rowspan="2"><img src="Images/US_NGO_logo.jpeg" width="100" height="100"></td>
  <td><font size="18">XDGNIRS</font></td>
</tr>
<tr>
  <td>Data reduction pipeline for <br>GNIRS cross-dispersed (XD) spectra</font></td>
</tr>
</table>


## **XDGNIRS Pipeline V3.0**
----

**Update [06/26/24]**

The latest version of the XDGNIRS code that is compatible with the new IRAF has been updated! The previous version could only run in $HOME, but that issue has been resolved. To run the pipeline in whichever directory you want, you must locate `.iraf/`. This is a hidden directory in $HOME, so you will need to run `ls -a` to view it. In this directory, there is a file called `login_dir`, which defines a variable called `start_dir`. Simply change the path to the desired location, and the XDGNIRS pipeline will be able to run in that directory. NOTE: You will need to change `start_dir` every time you want to run the XDGNIRS pipeline in a different directory. 

----
----

**Update [04/02/24]**

If you are using the new IRAF to run this pipeline, point your alias to **/code_iraf** rather than **/code**. Also, make sure that your fits files and inputfiles.lst are located in $HOME before running xdpiped. 

----

This repository contains the XDGNIRS pipeline, updated to work with Python 3.7.11. The code has been tested with the software installed on the latest version of the [Gemini IRAF Virtual Machine](https://gemini-iraf-vm-tutorial.readthedocs.io/en/stable/).

The full documentation, including installation instructions can be found on readthedocs:
- https://xdgnirs.readthedocs.io/en/latest/

### **Brief instructions for running the pipeline**
1. Move science and calibration data to the virtual machine.
2. Modify the input list so that the following data are included

   - #### ***Science target acquisition images***
     - Target names in headers (OBJECT keyword) must be identical to target name in spectroscopy file headers
     - This means that blind offset obsercations cannot be reduced without first editing the headers
     - Only the final acquisition image in the sequence is strictly necessary (the acquisition images are only needed becasue of the telescope position information in the file headers)
     #  
     
   - #### ***Science target spectroscopy files***
     - At least **two** object-sky pairs
     - ABBA, ABA, or ABAB nod pattern
     - Nod along slit of off to blank sky
     - Must contain a "q" offset (as well as or instead of "p" offset)
     #  
     
   - #### ***Standard star acquisition images***
     - Same criteria as for science target acquisition images
     #  
     
   - #### ***Standard star spectroscopy files***
     - **One** standard star observation only
     - Same criteria as for science target spectroscopy
     #  
     
   - #### ***One or more arcs***
   #  
   - #### ***A set of IR and QH flats***
   #  
   - #### ***One or more pinhole flats***
   #  

Note: The code can currently only deal with **one** standard star and **one** science target data set at a time.

3. Run the `XDpiped.csh` script using the following command
    
    `./Path/To/Script/XDpiped.csh inputfiles.lst` (or add the script to your `$PATH`. [See instructions](https://xdgnirs.readthedocs.io/en/latest/files/bashrc.html))

4. Inspect the final prodcuts that are located in the `PRODUCTS` directory

Note: Data taken before the 2012 lens replacement may contain many radiation events. To remedy this, make sure to set the despike_std parameter to 'yes' when running the pipeline.

---
## Need help?

Problems, comments, suggestions, and/or need help setting up and running the Jupyter notebooks? You can contact the US NGO members via our [Portal](http://ast.noao.edu/csdc/usngo), [Twitter](https://twitter.com/usngo), or submit a *New issue* through GitHub.

For assistance with running the GNIRSXD pipeline, please submit a ticket to the [Gemini Helpdesk](https://www.gemini.edu/observing/helpdesk/submit-general-helpdesk-request).

Follow us on Twitter! <a href="https://twitter.com/usngo" target="_blank"><img src="https://badgen.net/badge/icon/twitter?icon=twitter&label"></a>

---
