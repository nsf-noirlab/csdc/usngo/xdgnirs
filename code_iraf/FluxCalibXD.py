#!/usr/bin/env python
#
##
##
##	FluxCalibXD.py: basically a rewrite of Omaira's code by REM, to implement another approach to flux calibration 		
##
##

import sys
from pyraf import *
import astropy.io.fits
import numpy
from astropy.io.fits import getdata, getheader
import matplotlib.pyplot as plt
#iraf.reset(gemini="extern$gemini_public_1_11_1/")
iraf.gemini(_doprint=0, motd="no")
iraf.gnirs(_doprint=0)
iraf.imutil(_doprint=0)
iraf.onedspec(_doprint=0)
iraf.nsheaders('gnirs',Stdout='/dev/null')

#Define the combined science target data file
infile = sys.argv[1:2]
target = infile[0]
#Define the combined standard star data file
infile = sys.argv[2:3]
standards = infile[0]

#Does the user want us to calibrate full-slit spectra and spectra extracted in steps along the slit?
extras = sys.argv[3:4][0]
if extras == "yes":
    steps = int(sys.argv[4:5][0]) + 1

def add_units_to_headers(image,absolute_cal):
    #This is so we know whether we did absolute or relative flux cal
    if absolute_cal:
        iraf.hedit(images=image, fields='FUNITS', value='erg/cm2/s/A', add='yes', addonly='no', delete='no', verify='no', show='no', update='yes')
    else:
        iraf.hedit(images=image, fields='FUNITS', value='Flambda, relative', add='yes', addonly='no', delete='no', verify='no', show='no', update='yes')

#Multiply telluric-corrected orders by blackbody curves

import os
startdir = os.getcwd()
std_star_path = startdir + '/INTERMEDIATE/std_star.txt'
#file = open('std_star.txt','r')
file = open(std_star_path,'r')	


lines = file.readlines()
#Extract stellar temperature from std_star.txt file , for use in making blackbody
star_kelvin = float(lines[0].replace('\n','').split()[3])
#Extract K mag from std_star.txt file and convert to erg/cm2/s/A, for a rough flux scaling
star_Kmag = lines[0].replace('\n','').split()[2]
try:
    #find out if a K band mag exists in std_star.txt
    star_Kmag = float(star_Kmag)
    flambda = 10**(-star_Kmag/2.5) * 4.28E-11
    absolute_cal = True
except:
    #if not then just set to 1; no absolute flux cal. attempted
    flambda = 1
    absolute_cal = False
#account for standard star/science target exposure times
#EXPTIME keyword is the "Exposure time (s) for sum of all coadds"
std_exp = iraf.hselect(images='v'+standards+"[0]", field='EXPTIME', expr='yes', missing='INDEF', mode='al', Stdout=1)
tgt_exp = iraf.hselect(images='v'+target+"[0]", field='EXPTIME', expr='yes', missing='INDEF', mode='al', Stdout=1)
flambda = flambda * (float(std_exp[0].replace("'","")) / float(tgt_exp[0].replace("'","")))

listaA = listaB = listaC = ''
step_dict = {}
j = 1
#effectively, for every order (?); every line in standard star band/mag/T file
for line in lines:
    #find the start and end wavelengths of the spectral order
    wstart = iraf.hselect(images='v'+standards+'[sci,'+str(j)+']', field='CRVAL1', expr='yes',  missing='INDEF', mode='al', Stdout=1)
    wstart = float(wstart[0].replace("'",""))
    wdelt = iraf.hselect(images='v'+standards+'[sci,'+str(j)+']', field='CD1_1', expr='yes',  missing='INDEF', mode='al', Stdout=1)
    wend = wstart + (1022 * float(wdelt[0].replace("'","")))
    #make a blackbody
    iraf.mk1dspec(input="bbody"+(str(j)),output="bbody"+(str(j)),ap=1,rv=0.0,z='no',title='',ncols=1022,naps=1,header='',wstart=wstart,wend=wend,continuum=1000,slope=0.0,temperature=star_kelvin,fnu='no',lines='',nlines=0,profile='gaussian',peak=-0.5,gfwhm=20.0,lfwhm=20.0,seed=1,comments='yes',mode='ql')
    #scale it to the previous order's one (extensions 2-6) and multiply science target by it (all extractions)
    if j == 1:
        #for extension 1, roughly scale the blackbody to the standard star's K band flux (this is our very rough absolute flux calibration...)
        meana = iraf.imstat(images="bbody"+str(j), fields="mean", lower='INDEF', upper='INDEF', nclip=0, lsigma=3.0, usigma=3.0, binwidth=0.1, format='yes', cache='no', mode='al',Stdout=1)
        scalefac = flambda / float(meana[1].replace("'",""))
        iraf.imarith(operand1="bbody"+str(j), op="*", operand2=scalefac, result="bbscale"+str(j),title='',divzero=0.0,hparams='',pixtype='',calctype='',verbose='no',noact='no',mode='al')
        iraf.imarith(operand1="fvtell"+str(j), op="*", operand2="bbscale"+str(j), result="flam"+str(j),title='',divzero=0.0,hparams='',pixtype='',calctype='',verbose='no',noact='no',mode='al')
        #record flux density units in headers (i.e. whether absolute or relative cal. done)
        add_units_to_headers("flam"+str(j), absolute_cal)
        if extras == "yes":
            iraf.imarith(operand1="fatell"+str(j), op="*", operand2="bbscale"+str(j), result="flamfull"+str(j),title='',divzero=0.0,hparams='',pixtype='',calctype='',verbose='no',noact='no',mode='al')
            add_units_to_headers("flamfull"+str(j), absolute_cal)
            for k in range (1, steps):
                iraf.imarith(operand1="fs"+str(k)+"tell"+str(j), op="*", operand2="bbscale"+str(j), result="flamstep"+str(k)+"_"+str(j),title='',divzero=0.0,hparams='',pixtype='',calctype='',verbose='no',noact='no',mode='al')
                add_units_to_headers("flamstep"+str(k)+"_"+str(j), absolute_cal)
    else:
        #for other orders, find region of overlap with the previous order 
        #wstart is the first wavelength of the present order (i.e, the short-wavelength end of the region of overlap)
        wstart = iraf.hselect(images='bbody'+str(j-1), field='CRVAL1', expr='yes', Stdout=1)
        wstart = float(wstart[0].replace("'",""))
        #wend is the last wavelength of the previous order (i.e, the long-wavelength end of the region of overlap)
        wtemp = iraf.hselect('bbody'+str(j), field='CRVAL1', expr='yes',  missing='INDEF', mode='al', Stdout=1)
        wdelt = iraf.hselect('bbody'+str(j), field='CDELT1', expr='yes',  missing='INDEF', mode='al', Stdout=1)
        wend = float(wtemp[0].replace("'","")) + (1022 * float(wdelt[0].replace("'","")))
        #wend should be > wstart. If it's not, then there's something weird with the wavelength range of the data
        #this isn't necessarily a big problem, scientifically, but FluxCalibXD.py as written can't handle it
        #so check for this and exit if there is no overlap between any two orders
        if wend < wstart:
            print( "ERROR: Extensions "+str(j)+" and "+str(j-1)+" do not overlap in wavelength. This is unusual, and suggests that the grating was not in the expected position. This may not be a problem for the scientific use of the data, but XDGNIRS cannot handle this and is not able to flux-calibrate the spectral orders. Please plot INTERMEDIATE/calibrated_arc.fits to see if the data cover the wavelength range you need.")
            sys.exit(0)
        #find mean in that wavelength region (using previously-scaled bbody)
        iraf.imdelete(images='tempa',go_ahead='yes',verify='no',default_acti='yes', mode='ql')
        iraf.imdelete(images='tempb',go_ahead='yes',verify='no',default_acti='yes', mode='ql')
        iraf.scopy(input='bbody'+str(j), output='tempa', w1=wstart, w2=wend,apertures='',bands='',beams='',apmodulus=0,format='multispec',renumber='no',offset=0,clobber='no',merge='no',rebin='yes',verbose='no',mode='ql')
        iraf.scopy(input='bbscale'+str(j-1), output='tempb', w1=wstart, w2=wend,apertures='',bands='',beams='',apmodulus=0,format='multispec',renumber='no',offset=0,clobber='no',merge='no',rebin='yes',verbose='no',mode='ql')
        meana = iraf.imstat(images='tempa', fields="mean",lower='INDEF', upper='INDEF', nclip=0, lsigma=3.0, usigma=3.0, binwidth=0.1, format='yes', cache='no', mode='al',Stdout=1)
        meanb = iraf.imstat(images='tempb', fields='mean',lower='INDEF', upper='INDEF', nclip=0, lsigma=3.0, usigma=3.0, binwidth=0.1, format='yes', cache='no', mode='al',Stdout=1)
        meana = float(meana[1].replace("'",""))
        meanb = float(meanb[1].replace("'",""))
        #scale current blackbody to that for the previous order using ratio of means
        scalefac = meanb/meana
        iraf.imarith (operand1='bbody'+str(j), op="*", operand2=scalefac, result='bbscale'+str(j),title='',divzero=0.0,hparams='',pixtype='',calctype='',verbose='no',noact='no',mode='al')
        #multiply telluric-corrected science target spectrum by scaled blackbody
        iraf.imarith(operand1="fvtell"+str(j), op="*", operand2="bbscale"+str(j), result="flam"+str(j),title='',divzero=0.0,hparams='',pixtype='',calctype='',verbose='no',noact='no',mode='al')
        #record flux density units in headers (i.e. whether absolute or relative cal. done)
        add_units_to_headers("flam"+str(j), absolute_cal)
        #also calibrate the full-slit and stepped-along-the slit spectra, if requested
        if extras == "yes":
            iraf.imarith(operand1="fatell"+str(j), op="*", operand2="bbscale"+str(j), result="flamfull"+str(j),title='',divzero=0.0,hparams='',pixtype='',calctype='',verbose='no',noact='no',mode='al')
            add_units_to_headers("flamfull"+str(j), absolute_cal)
            for k in range (1, steps):
                iraf.imarith(operand1="fs"+str(k)+"tell"+str(j), op="*", operand2="bbscale"+str(j), result="flamstep"+str(k)+"_"+str(j),title='',divzero=0.0,hparams='',pixtype='',calctype='',verbose='no',noact='no',mode='al')
                add_units_to_headers("flamstep"+str(k)+"_"+str(j), absolute_cal)
	
    j = int(j) +1

#At this stage have individual, flux-calibrated orders

file.close()
