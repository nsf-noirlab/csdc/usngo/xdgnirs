#Check for mistyped or otherwise incorrect input keywords

import sys
import string

nkeys = sys.argv[1]
input_values = []

for i in range(2, int(nkeys)+2):
    input_values.append(sys.argv[i])

allowed_values = (["step", "stop", "rawdir", "cleanir_sciflag", "cleanir_stdflag", "cleanir_arcflag", "cleanir_IRflag", "cleanir_QHflag", "cleanir_pinflag","nsflat_inter", "despike_tgt", "despike_std", "tgt_thresh", "std_thresh", "nssdist_inter", "nswav_inter", "nsfit_inter", "nsext_inter", "use_apall", "aperture", "extras", "columns", "continuum_inter", "telluric1_inter", "telluric2_inter", "hlines", "shift_to_rest", "offsets", "error_spectrum", "gemiraf_ver", "wave_soln_linear"])

for keyword in input_values:
    keyword = keyword.split('=')
    if keyword[0] not in allowed_values:
        print( "ERROR: '",keyword[0],"' is not an allowed keyword. Type xdpiped -h to see the available options.")
