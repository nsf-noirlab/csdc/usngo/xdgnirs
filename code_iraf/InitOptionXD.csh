#!/bin/bash
#
##
#####  Including starting options of the pipelie
##  O. Gonzalez Martin (May 2012).

set namekey = $1
set startdir = $2
set aux =  `cat ${startdir}/LISTS/entries.txt | grep ${namekey}`
#What to do if an option is undefined on command line (i.e., how to set defaults)
if ($aux == "") then
	if (${namekey} == "step") then	
		set entryout = 0
	else
		if (${namekey} == "stop") then
			set entryout = 13
		else if (${namekey} == "rawdir") then
			set entryout = './'
                else if (${namekey} == "cleanir_sciflag") then
			set entryout = 'f'
		else if (${namekey} == "cleanir_stdflag") then
			set entryout = 'f'
		else if (${namekey} == "cleanir_arcflag") then
			set entryout = 'n'
		else if (${namekey} == "cleanir_IRflag") then
			set entryout = 'n'
		else if (${namekey} == "cleanir_QHflag") then
			set entryout = 'n'
		else if (${namekey} == "cleanir_pinflag") then
			set entryout = 'n'
		else if (${namekey} == "tgt_thresh") then
			set entryout = 20
		else if (${namekey} == "std_thresh") then
			set entryout = 50
		else if (${namekey} == "aperture") then
			set entryout = 4
		else if (${namekey} == "columns") then
			set entryout = 6
		else if (${namekey} == "hlines") then
			set entryout = 'linefit_auto'
		else if (${namekey} == "gemiraf_ver") then
			set entryout = '1.12+'
		else if (${namekey} == "ureka") then
			set entryout = 'yes'
                else if (${namekey} == "despike_tgt") then
			set entryout = 'yes'
		else if (${namekey} == "wave_soln_linear") then
			set entryout = 'yes'
		else 		
			set entryout = 'no'	
		endif
	endif	
else
	set entryout =  `echo $aux | cut -f2 -d"="`
endif
echo $entryout
