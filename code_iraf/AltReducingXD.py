#!/usr/bin/pyraf_python
#New code to use GEMCRSPEC wrapper for LACOSMIC to remove spikes in GNIRS 2D data
#REM Dec 2015
#
import sys
import shutil
from pyraf import *
import astropy.io.fits
import numpy
from astropy.io.fits import getdata, getheader
#iraf.reset(gemini="extern$gemini_public_1_11_1/")
iraf.gemini(_doprint=0, motd="no")
iraf.gnirs(_doprint=0)
iraf.imutil(_doprint=0)
iraf.nsheaders('gnirs',Stdout='/dev/null')
iraf.unlearn(iraf.jimexam)

files = open(sys.argv[1:2][0], 'r')
despike = sys.argv[2:3][0]
gemiraf = sys.argv[3:4][0]
configdir = sys.argv[4:5][0]
errorspec = sys.argv[5:6][0]
iraf.task (lacos_spec = configdir+'lacos_spec.cl')

if despike == 'yes':
    #Remove spikes from data using gemcrspec wrapper to lacosmic task
    #Only doing this for science target, which usually has long enough exposures that there are some spikes
    for line in files:
        iraf.delete ('temp2.fits')
        iraf.delete('temp.fits')
        line = 'nc'+line.replace('\n','')
        shutil.copyfile(line, 'temp.fits')
        #Current version of gemcrspec only accepts files with dispersion in horizontal direction, so need to use imtranspose
        iraf.imtranspose(input=line+'[sci,1]', output='temp.fits[sci,1,overwrite]')
        iraf.imtranspose(input=line+'[var,1]', output='temp.fits[var,1,overwrite]')
        iraf.imtranspose(input=line+'[dq,1]', output='temp.fits[dq,1,overwrite]')
        iraf.hedit(images='temp.fits[sci,1]', fields='dispaxis', value='1', add='no', addonly='no', delete='no', ver='no', show='no', update='yes')
        iraf.gemcrspec (inimages='temp.fits', outimages='temp2.fits', xorder='0', yorder='0', sigclip='4.5', sigfrac='0.5', objlim='5.0', niter='4', fl_vardq='yes', sci_ext='SCI', var_ext='VAR', dq_ext='DQ', key_ron='RDNOISE', key_gain='GAIN', ron='3.5', gain='2.2', logfile='', verbose='yes', status='0')
        shutil.copyfile('temp2.fits', 'l'+line)
        iraf.imtranspose(input='temp2.fits[sci,1]', output='l'+line+'[sci,1,overwrite]')
        iraf.imtranspose(input='temp2.fits[var,1]', output='l'+line+'[var,1,overwrite]')
        iraf.imtranspose(input='temp2.fits[dq,1]', output='l'+line+'[dq,1,overwrite]')
        iraf.hedit(images='l'+line+'[sci,1]', fields='dispaxis', value='2', add='no', addonly='no', delete='no', ver='no', show='no', update='yes')
        pref = 'lnc'
        reduce_pref='r'
else:
    #Haven't found parameters that work well for standard stars, and they generally have few spikes anyway, so just set the file prefix appropriately for nsreduce
    pref = 'nc'
    reduce_pref = 'rl'

#Now reduce the radiation event-cleaned data (flatfield, sky subtract, ...); all files (both beams)
#Also reduce the data without sky subtraction, so can use sky for noise calculation later (if desired)

#create the file list that will be passed to nsreduce
f = []
files.seek(0)
for line in files:
    line = line.replace('\n','')
    f.append(pref+line)
objString = ",".join(f)
files.close()

#nsreduce parameters slightly different between Gemini IRAF 1.11.1 and 1.12beta
if gemiraf == "1.11.1":
           iraf.nsreduce(inimages=objString,outprefix = reduce_pref,fl_sky='yes',skyrange='INDEF',fl_flat='yes',fl_nsappwave='no',flatimage='MasterFlat.fits',outimages = "",fl_nscut = 'yes',section = "",fl_corner = 'yes',fl_process_cut = 'yes',nsappwavedb = configdir+"config/nsappwave.fits",crval = "INDEF",cdelt = "INDEF",fl_dark = 'no',darkimage = "",fl_save_dark = 'no',skyimages = "",skysection = "",combtype = "median",rejtype = "avsigclip",masktype = "goodvalue",maskvalue = 0.,scale = "none",zero = "median",weight = "none",statsec = "[*,*]",lthreshold = "INDEF", hthreshold = "INDEF",nlow = 1,  nhigh = 1,nkeep = 0,mclip = 'yes',lsigma = 3., hsigma = 3.,snoise = "0.0",sigscale = 0.1,pclip = -0.5,grow = 0.0,nodsize = 3.,flatmin = 0.0,fl_vardq = 'yes',logfile = "",verbose = 'yes',debug = 'no',force = 'no')
           if errorspec == 'yes':
                iraf.nsreduce(inimages=objString,outprefix ="k",fl_sky='no',skyrange='INDEF',fl_flat='yes',fl_nsappwave='no',flatimage='MasterFlat.fits',outimages = "",fl_nscut = 'yes',section = "",fl_corner = 'yes',fl_process_cut = 'yes',nsappwavedb = configdir+"config/nsappwave.fits",crval = "INDEF",cdelt = "INDEF",fl_dark = 'no',darkimage = "",fl_save_dark = 'no',skyimages = "",skysection = "",combtype = "median",rejtype = "avsigclip",masktype = "goodvalue",maskvalue = 0.,scale = "none",zero = "median",weight = "none",statsec = "[*,*]",lthreshold = "INDEF", hthreshold = "INDEF",nlow = 1,  nhigh = 1,nkeep = 0,mclip = 'yes',lsigma = 3., hsigma = 3.,snoise = "0.0",sigscale = 0.1,pclip = -0.5,grow = 0.0,nodsize = 3.,flatmin = 0.0,fl_vardq = 'yes',logfile = "",verbose = 'yes',debug = 'no',force = 'no')

else:
           iraf.nsreduce(inimages=objString,outprefix = reduce_pref,fl_sky='yes',skyrange='INDEF',fl_flat='yes',fl_nsappwave='no',flatimage='MasterFlat.fits',outimages = "",fl_cut = 'yes',section = "",fl_corner = 'yes',fl_process_cut = 'yes',nsappwavedb = configdir+"config/nsappwave.fits",crval = "INDEF",cdelt = "INDEF",fl_dark = 'no',darkimage = "",fl_save_dark = 'no',skyimages = "",skysection = "",combtype = "median",rejtype = "avsigclip",masktype = "goodvalue",maskvalue = 0.,scale = "none",zero = "median",weight = "none",statsec = "[*,*]",lthreshold = "INDEF", hthreshold = "INDEF",nlow = 1,  nhigh = 1,nkeep = 0,mclip = 'yes',lsigma = 3., hsigma = 3.,snoise = "0.0",sigscale = 0.1,pclip = -0.5,grow = 0.0,nodsize = 3.,flatmin = 0.0,fl_vardq = 'yes',logfile = "",verbose = 'yes',debug = 'no',force = 'no') 
           if errorspec == 'yes':
               iraf.nsreduce(inimages=objString,outprefix ="k",fl_sky='no',skyrange='INDEF',fl_flat='yes',fl_nsappwave='no',flatimage='MasterFlat.fits',outimages = "",fl_cut = 'yes',section = "",fl_corner = 'yes',fl_process_cut = 'yes',nsappwavedb = configdir+"config/nsappwave.fits",crval = "INDEF",cdelt = "INDEF",fl_dark = 'no',darkimage = "",fl_save_dark = 'no',skyimages = "",skysection = "",combtype = "median",rejtype = "avsigclip",masktype = "goodvalue",maskvalue = 0.,scale = "none",zero = "median",weight = "none",statsec = "[*,*]",lthreshold = "INDEF", hthreshold = "INDEF",nlow = 1,  nhigh = 1,nkeep = 0,mclip = 'yes',lsigma = 3., hsigma = 3.,snoise = "0.0",sigscale = 0.1,pclip = -0.5,grow = 0.0,nodsize = 3.,flatmin = 0.0,fl_vardq = 'yes',logfile = "",verbose = 'yes',debug = 'no',force = 'no') 





