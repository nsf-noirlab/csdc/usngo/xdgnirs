
def specvar(inlist, ap_width=10, clobber=False):
    """
    Attempts to build a variance spectrum of multiple exposures from
    the same source.

    Parameters
    ----------
    inlist : list of strings
        List of cross-dispersed exposures with the 'ttfrlnc' prefix.
    ap_width : integer
        Width in pixels of aperture to be extracted.
    clobber : boolean
        Overwrites FITS images.

    Returns
    -------
    snr : numpy.ndarray
        Signal to noise ratio arrays for each order.

    Description
    -----------
        This function was developed to build a variance spectrum from
    multiple exposures of cross-dispersed spectra. It was motivated
    by the apparent discrepancy in the signal to noise ratio (SNR)
    assessment function, which shows a wavelength dependency.
    """

    # Loading iraf modules.
    iraf.noao()
    iraf.twodspec()
    iraf.apextract()

print 'here'
test = specvar(ttfrlncN20150917S0087.fits,ttfrlncN20150917S0088.fits,ttfrlncN20150917S0089.fits,ttfrlncN20150917S0090.fits)
