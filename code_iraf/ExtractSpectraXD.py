#!/usr/bin/env pyraf_python
#

import sys, shutil, os
from pyraf import *
import astropy.io.fits
import numpy
from astropy.io.fits import getdata, getheader
#iraf.reset(gemini="extern$gemini_public_1_11_1/")
iraf.gemini(_doprint=0, motd="no")
iraf.gnirs(_doprint=0)
iraf.imutil(_doprint=0)
iraf.nsheaders('gnirs',Stdout='/dev/null')

target = sys.argv[1:2][0]
standard = sys.argv[2:3][0]
nsextinter = sys.argv[3:4][0]
aperture_minus = sys.argv[4:5][0]
aperture_plus = sys.argv[5:6][0]
extras = sys.argv[6:7][0]
step = int(sys.argv[7:8][0])

gemiraf = sys.argv[8:9][0]
path_to_nsextract = sys.argv[9:10][0]
if gemiraf == "1.11.1":
        #Use the version of nsextract.cl that comes as standard with 1.12beta
	#Interpretation of "upper" and "lower" in previous versions was weird
        iraf.task (nsextract = path_to_nsextract+'nsextract.cl')

sky = sys.argv[10:11][0]
errorspec = sys.argv[11:12][0]

use_apall = sys.argv[12:13][0]

def column_lookup(dbfile):
    peak = []
    for i in range (1,7):
        if os.path.isfile(dbfile+str(i)+'_'):
            apfile = open(dbfile+str(i)+'_', 'r') 
            for line in apfile:
                if 'center' in line:
                    peak.append(float(line.split()[1]))
                    break
        else:
            peak.append('not found')
    return peak

#**************EXTRACT SCIENCE TARGET AND STANDARD STAR IN USER-DEFINED APERTURE*********************

if use_apall == 'no':
    #Just extract columns from straightened 2D images
    #Standard star
    iraf.nsextract(inimages=standard,outspectra='',outprefix='v',dispaxis=1,database='',line=700,nsum=10,ylevel='INDEF',upper=aperture_plus,lower=aperture_minus,background='none',fl_vardq='yes',fl_addvar='no',fl_skylines='yes',fl_inter=nsextinter,fl_apall='no',fl_trace='no',aptable=path_to_nsextract+'config/apertures.fits',fl_usetabap='no',fl_flipped='yes',fl_project='yes',fl_findneg='no',bgsample='*',trace='',tr_nsum=10,tr_step=10,tr_nlost=3,tr_function='legendre',tr_order=5,tr_sample='*',tr_naver=1,tr_niter=0,tr_lowrej=3.0,tr_highrej=3.0,tr_grow=0.0,weights='variance',logfile='',verbose='yes',mode='al')
    #Science target
    iraf.nsextract(inimages=target,outspectra='',outprefix='v',dispaxis=1,database='',line=700,nsum=10,ylevel='INDEF',upper=aperture_plus,lower=aperture_minus,background='none',fl_vardq='yes',fl_addvar='no',fl_skylines='yes',fl_inter=nsextinter,fl_apall='no',fl_trace='no',aptable=path_to_nsextract+'config/apertures.fits',fl_usetabap='no',fl_flipped='yes',fl_project='yes',fl_findneg='no',bgsample='*',trace='',tr_nsum=10,tr_step=10,tr_nlost=3,tr_function='legendre',tr_order=5,tr_sample='*',tr_naver=1,tr_niter=0,tr_lowrej=3.0,tr_highrej=3.0,tr_grow=0.0,weights='variance',logfile='',verbose='yes',mode='al')
else:
    #Use apall to do a weighted extraction
    #Not using background subtraction b/c couldn't find good standard parameters for that (also not appropriate for extended objects)
    #Standard star
    iraf.nsextract(inimages=standard,outspectra='',outprefix='v',dispaxis=1,database='',line=700,nsum=20,ylevel='INDEF',upper=aperture_plus,lower=aperture_minus,background='none',fl_vardq='yes',fl_addvar='no',fl_skylines='yes',fl_inter=nsextinter,fl_apall='yes',fl_trace='no',aptable=path_to_nsextract+'config/apertures.fits',fl_usetabap='no',fl_flipped='yes',fl_project='yes',fl_findneg='no',bgsample='*',trace='',tr_nsum=10,tr_step=10,tr_nlost=3,tr_function='legendre',tr_order=5,tr_sample='*',tr_naver=1,tr_niter=0,tr_lowrej=3.0,tr_highrej=3.0,tr_grow=0.0,weights='variance',logfile='',verbose='yes',mode='al')
    #Science target
    iraf.nsextract(inimages=target,outspectra='',outprefix='v',dispaxis=1,database='',line=700,nsum=20,ylevel='INDEF',upper=aperture_plus,lower=aperture_minus,background='none',fl_vardq='yes',fl_addvar='no',fl_skylines='yes',fl_inter=nsextinter,fl_apall='yes',fl_trace='no',aptable=path_to_nsextract+'config/apertures.fits',fl_usetabap='no',fl_flipped='yes',fl_project='yes',fl_findneg='no',bgsample='*',trace='',tr_nsum=10,tr_step=10,tr_nlost=3,tr_function='legendre',tr_order=5,tr_sample='*',tr_naver=1,tr_niter=0,tr_lowrej=3.0,tr_highrej=3.0,tr_grow=0.0,weights='variance',logfile='',verbose='yes',mode='al')

    

#******************CHECK THAT NSEXTRACT LOCATED A PLAUSIBLE PEAK*********************

#Only if the user didn't run nsextract interactively:
#Check that nsextract located the peak of the target in a sensible place
#For faint targets it often finds a noise peak instead; if so, want to re-extract at right location
#(sometimes the peak is so close to the end of the slit that the extraction fails with an "aperture too large error")
#So:
#  first, check that all the extensions have been extracted
#  then, look at the database record of the location of the standard star peak
#        look up the science target and standard star q offsets and figure out if the relative location of the target peak was correct
#        if not, re-extract at the expected location

if nsextinter== 'no':
    std_peak = column_lookup('database/apstandard_comb_SCI_') 
    tgt_peak = column_lookup('database/aptarget_comb_SCI_')
      
    ####
    #TEST
    #I am going to remove the '..'
    posfile = open ('LISTS/qoffsets.txt')
    ####
    #posfile = open ('../LISTS/qoffsets.txt')
    tgtq = float(posfile.readline())
    stdq = float(posfile.readline())
    obs = astropy.io.fits.open(target)
    pixscale = obs[0].header["PIXSCALE"]
    diff = (tgtq - stdq)/pixscale 
    reExtract = False
    found_spectrum = []
    #nsextract should find spectrum within 'tolerance' pixels of expected location
    #depends on how well the observer centred the target along the slit. 5 pix is an initial guess at what's reasonable.
    #(rather than an offset, would be better to use some measure of whether the peak found by nsextract was real, e.g. counts + FWHM. Not recorded in database, though.)
    tolerance = 5 
    for i in range (0,6):
        expected = '%s' % float('%.4g' % (std_peak[i] + diff))
        if tgt_peak[i] == 'not found':
            print( 'In extension '+str(i+1)+' nscombine did not extract anything. Re-extracting with the aperture forced to be at '+expected)
            found_spectrum.append(0)
            reExtract=True
        else:    
            found = '%s' % float('%.4g' % tgt_peak[i])
            if abs((tgt_peak[i] - diff) - std_peak[i]) < tolerance:
                print( '***CHECK: In extension '+str(i+1)+' nscombine detected the target spectrum close to the expected location along slit (x = '+found+' vs expected x = '+expected+')')
                found_spectrum.append(1)
            else:
                print( '***WARNING: In extension '+str(i+1)+' nscombine extracted an unexpected location along the slit (x = '+found+' vs expected x = '+expected+'). It is probably extracting noise; re-extracting with the aperture forced to be at the expected location.')
                found_spectrum.append(0)
                reExtract=True
         
    if reExtract:        
        #Re-extract science target spectrum if needed
        for i in range (0,6):
            #create new aperture files in database   
            #ran into trouble when only replacing ones that weren't well centred, so just replacing them all 
            #(but using nsextract peak for target when it seemed to be in the right place)
            if os.path.isfile('database/aptarget_comb_SCI_'+str(i+1)+'_'):
                os.remove ('database/aptarget_comb_SCI_'+str(i+1)+'_')
            apfile = open ('database/apstandard_comb_SCI_'+str(i+1)+'_', 'r')
            newapfile = open ('database/apnewstandard_comb_SCI_'+str(i+1)+'_', 'w')
            if found_spectrum[i] == 0:
                clean  = apfile.read().replace(str(std_peak[i]), str(std_peak[i] + diff)+' ').replace('standard_comb','newstandard_comb')
            else:
                clean  = apfile.read().replace(str(std_peak[i]), str(tgt_peak[i])+' ').replace('standard_comb','newstandard_comb')
            newapfile.write(clean)
            apfile.close()
            newapfile.close()
        shutil.copy(standard, 'new'+standard)
        iraf.imdelete(images='v'+target)
        #Using these settings in nsextract will force it to use the aperture size and centre in the edited "apnewstandard_comb" files
        iraf.nsextract(inimages=target,outspectra='',outprefix='v',dispaxis=1,database='',line=700,nsum=20,ylevel='INDEF',upper=aperture_plus,lower=aperture_minus,background='none',fl_vardq='yes',fl_addvar='no',fl_skylines='yes',fl_inter=nsextinter,fl_apall='yes',fl_trace='no',aptable=path_to_nsextract+'config/apertures.fits',fl_usetabap='no',fl_flipped='yes',fl_project='yes',fl_findneg='no',bgsample='*',trace='newstandard_comb',tr_nsum=10,tr_step=10,tr_nlost=3,tr_function='legendre',tr_order=5,tr_sample='*',tr_naver=1,tr_niter=0,tr_lowrej=3.0,tr_highrej=3.0,tr_grow=0.0,weights='variance',logfile='',verbose='yes',mode='al')

    #Slight complication - we occasionally find that nsextract locates the aperture too close to the end of the slit
    #Then it exits with an "Aperture too large" error and spectra aren't extracted for one or more extensions
    #But we work around that above, so when we check for errors in XDpiped.csh, we ignore this error
    #But maybe that error can happen for other reasons
    #So to be on the safe side, will check that all extensions are present in the extracted target file (should really add other files as well...)
    check_extn = iraf.gemextn(inimages=target,check='exists,mef',process='expand',index='',extname='SCI',extversion='',ikparams='',omit='',replace='',outfile='STDOUT',logfile='',Stdout=1,glogpars='',verbose='no',fail_count='0', count='20', status='0')
    if len(check_extn) != 6:
        print( "ERROR: target_comb file contains only ", len(check_extn), 'extensions. Exiting script.')


#******************FULL-SLIT, STEPWISE, AND SKY EXTRACTIONS*********************
    
#Now get on with extracting a sky spectrum (if needed to make an error spectrum)
#Also extract the "extras" (full slit, step-by-step) if user set that option
#THESE THINGS HAVE NOT BEEN TESTED/UPDATED IN A LONG TIME; USE WITH CAUTION
    
#Sky 
if errorspec == 'yes':
    iraf.nsextract(inimages=sky,outspectra='',outprefix='v',dispaxis=1,database='',line=700,nsum=20,ylevel='INDEF',upper=aperture_plus,lower=aperture_minus,background='none',fl_vardq='yes',fl_addvar='no',fl_skylines='yes',fl_inter=nsextinter,fl_apall='no',fl_trace='no',aptable=path_to_nsextract+'config/apertures.fits',fl_usetabap='no',fl_flipped='yes',fl_project='yes',fl_findneg='no',bgsample='*',trace='',tr_nsum=10,tr_step=10,tr_nlost=3,tr_function='legendre',tr_order=5,tr_sample='*',tr_naver=1,tr_niter=0,tr_lowrej=3.0,tr_highrej=3.0,tr_grow=0.0,weights='variance',logfile='',verbose='yes',mode='al')        

if extras == "yes":
    #****Approx. full-slit extraction (science target only)
    #Uses +/-23 pix aperture (6.9", almost whole length of slit), appropriate for objects centred along length of slit (q=0)
    #Not sure what the effect is if nsextract finds a spectrum that's not centred along the slit

    print( "***WARNING: full-slit and stepwise extractions haven't been used or tested much; proceed with caution and double-check your results.")

    iraf.nsextract(inimages=target,outspectra='',outprefix='a',dispaxis=1,database='',line=700,nsum=20,ylevel='INDEF',upper=23,lower=-23,background='none',fl_vardq='yes',fl_addvar='no',fl_skylines='yes',fl_inter=nsextinter,fl_apall='no',fl_trace='no',aptable=path_to_nsextract+'config/apertures.fits',fl_usetabap='no',fl_flipped='yes',fl_project='yes',fl_findneg='no',bgsample='*',trace='',tr_nsum=10,tr_step=10,tr_nlost=3,tr_function='legendre',tr_order=5,tr_sample='*',tr_naver=1,tr_niter=0,tr_lowrej=3.0,tr_highrej=3.0,tr_grow=0.0,weights='variance',logfile='',verbose='yes',mode='al')

    #****Extraction in steps either side of the peak 
	#Calling apall this time, tracing the peak first to make sure the same part of the object is extracted in each step along the slit for all orders (needed when there is complex, spectrally-varying structure in a galaxy, for example; otherwise the spectra can have offsets between orders)
    
	#This first nsextract step, outside the loop, gets the trace into the database to be used when do the "real" extraction
    iraf.nsextract(inimages=target,outspectra='trace_ref',outprefix='x',dispaxis=1,database='',line=700,nsum=20,ylevel='INDEF',upper=3,lower=-3,background='none',fl_vardq='yes',fl_addvar='no',fl_skylines='yes',fl_inter=nsextinter,fl_apall='yes',fl_trace='yes',aptable=path_to_nsextract+'config/apertures.fits',fl_usetabap='no',fl_flipped='yes',fl_project='no',fl_findneg='no',bgsample='*',trace='',tr_nsum=10,tr_step=10,tr_nlost=3,tr_function='legendre',tr_order=5,tr_sample='300:1000',tr_naver=1,tr_niter=0,tr_lowrej=3.0,tr_highrej=3.0,tr_grow=0.0,weights='variance',logfile='',verbose='yes',mode='al')
    
    #This step is never done interactively, because it uses info from the previous call (and it would be very tedious)
    n = 0
    for j in range (-21,21,step):
        n = n + 1
        iraf.nsextract(inimages=target,outspectra='',outprefix='s'+str(n),dispaxis=1,database='',line=700,nsum=20,ylevel='INDEF',upper=j+step,lower=j,background='none',fl_vardq='yes',fl_addvar='no',fl_skylines='yes',fl_inter="no",fl_apall='no',fl_trace='no',aptable=path_to_nsextract+'config/apertures.fits',fl_usetabap='no',fl_flipped='yes',fl_project='yes',fl_findneg='no',bgsample='*',trace='',tr_nsum=10,tr_step=10,tr_nlost=3,tr_function='legendre',tr_order=5,tr_sample='*',tr_naver=1,tr_niter=0,tr_lowrej=3.0,tr_highrej=3.0,tr_grow=0.0,weights='variance',logfile='',verbose='yes',mode='al')
		
    nsteps = open ("nsteps.txt", "w")
    nsteps.write (str(n))
    nsteps.close()



