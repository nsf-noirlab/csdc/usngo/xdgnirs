#!/usr/bin/env pyraf_python
#
##
##
##	CombineOrders.py: looks for offsets between orders, combines orders -REM
##      NO ORDER SCALING CURRENTLY DONE ON STEPWISE (OR FULL-SLIT) EXTRACTIONS
##      OFFSETS=MANUAL WILL ONLY ALLOW USER TO SCALE ORDERS IN "STANDARD" EXTRACTION
##      I.E. CENTRAL ONE OF WIDTH DEFINED BY "APERTURE" PARAMETER
#
# 2015.09.22: (jmiller,mpohlen) import matplotlib to avoid (in cron)
#             'tkinter.TclError: no display name and no $DISPLAY' error    
#
# Import libraries
#

import os
import os.path
import sys
from pyraf import *
import astropy.io.fits
import numpy
from astropy.io.fits import getdata, getheader
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
#Path for importing order combining code
procdir = sys.argv[0:1][0].split('//')[0]
sys.path.append(procdir)
import gnirs_unc

#iraf.reset(gemini="extern$gemini_public_1_11_1/")
iraf.gemini(_doprint=0, motd="no")
iraf.gnirs(_doprint=0)
iraf.imutil(_doprint=0)
iraf.onedspec(_doprint=0)
iraf.nsheaders('gnirs',Stdout='/dev/null')

def plot_orders (file, extension, colour, ax):
    region = initcut[extension-1]
    region = str(region).split()
    start = int(region[0])
    end = int(region[1])
    specdata = open(file)
    xdata = []
    ydata = []
    for line in specdata:
        vals = list(map(float, line.split()))

        xdata.append(vals[0]/10000)
        ydata.append(vals[1])
    ax.plot (xdata[start:end], ydata[start:end], '-', color=colour, alpha=0.7)

def make_orders_fig (what, name):
    fig = plt.figure ()
    ax = fig.add_subplot(111)
    plot_orders (what+str(1)+'.txt', 1, 'black', ax)
    plot_orders (what+str(2)+'.txt', 2, 'blue', ax)
    plot_orders (what+str(3)+'.txt', 3, 'black', ax)
    plot_orders (what+str(4)+'.txt', 4, 'blue', ax)
    plot_orders (what+str(5)+'.txt', 5, 'black', ax)
    plot_orders (what+str(6)+'.txt', 6, 'blue', ax)
    ylim = iraf.imstat(images=what+str(3), fields="midpt", lower=0, upper="INDEF", nclip=0, lsigma=3, usigma=3, binwidth=0.1, format="yes", Stdout=1)
    ylim = ylim[1]
    ax.set_ylim(bottom=0, top=float(ylim)*2)
    ax.set_xlim (0.82, 2.55)
    plt.xlabel (r"$\mu$m, observed")
    plt.ylabel (r"F$_{\lambda}$")
    plt.savefig(name)

#Target redshift, and whether to use it
print('redshift = ',sys.argv[1:2][0])
print('doshift  = ',sys.argv[2:3][0])

redshift = sys.argv[1:2][0]
doshift = sys.argv[2:3][0]

try:
    #Test whether there's a real number in there (not a redshift not found message, for example)
    testvar = float(redshift)
except:
    print("setting doshift variable to \"no\" due to bad redshift value")
    doshift = 'no'

#Full-slit and step-by-step extractions done? How many steps?
extras = sys.argv[3:4][0]
if extras == "yes":
    steps = int(sys.argv[4:5][0]) + 1
#Correct for order offsets? How?
offsets = sys.argv[5:6][0]

#Error spectrum?
error_spectrum = sys.argv[6:7][0]

#XDGNIRS version
version = sys.argv[7:8][0] + ' ' + sys.argv[8:9][0]

#get target name from headers, but replace special characters with ones that won't cause trouble
#(only come across "/" and "space" so far)
obs=astropy.io.fits.open('vtarget_comb.fits')
target_name =  obs[0].header["OBJECT"].replace("/","_").replace(" ","")

#---
startdir = os.getcwd()
goodbits_path = startdir + '/INTERMEDIATE/goodbits.txt'
#file = open('goodbits.txt','r')
file = open(goodbits_path,'r')

initcut = file.readlines()
file.close()
listaA = listaB = listaC = ''
step_dict = {}
start = []
end = []

#First, need to know about the "good" bits of the orders from the goodbits file
#This file records the range of pixels in each order that should be incorporated into the final, merged spectrum
#Contains best guesses and can be edited by user
for j in range (1,7):
    start.append(initcut[j-1].split()[0])
    end.append(initcut[j-1].split()[1])

    if offsets == "no":
        #not attempting to make any correction for offsets between orders
        #easiest thing to do is just to copy flam* to final*, so files all have same name later
        iraf.imcopy(input='flam'+str(j), output='final'+str(j), verbose='no')

if offsets == "manual":
    #Allow the user to attempt to adjust the relative fluxes of each order before combining them
    print( "Entering specplot so user can adjust order scaling as necessary")
    spectra = 'flam1['+str(start[0])+':'+str(end[0])+'],flam2['+str(start[1])+':'+str(end[1])+'],flam3['+str(start[2])+':'+str(end[2])+'],flam4['+str(start[3])+':'+str(end[3])+'],flam5['+str(start[4])+':'+str(end[4])+'],flam6['+str(start[5])+':'+str(end[5])+']'
    iraf.specplot (spectra=spectra,autolayout='no',logfile='offsets.log',apertures='',bands='',autoscale='yes',frac=1.0,units='',scale=1,offset=0,step=0,ptype=1,labels='user',ulabels='',xlpos=1.02,ylpos=0.0,sysid='yes',yscale='yes',title='',xlabel='',ylabel='',xmin='INDEF',ymin='INDEF',xmax='INDEF',ymax='INDEF',graphics='stdgraph',cursor='')
    file = open('offsets.log','r')
    offsets = file.readlines()
    for j in range (1,7):
    #locate the scaling of the order that is written into offsets.log, and multiply by it
        scale = float(offsets[j+4].split()[5])
        iraf.imarith(operand1="flam"+str(j)+".fits",op='*',operand2=scale,result="final"+str(j)+".fits",title='',divzero=0.0,hparams='',pixtype='',calctype='',verbose='no',noact='no')
    

#Construct lists of files to go into odcombine and the gnirs_unc.joinorders_noresampling
temp = []
for j in range (1,7):
    temp.append('final'+str(j)+'.fits')
    if j == 1:
        listaB += 'final'+str(j)+'['+start[j-1] +':'+end[j-1]+']'
        if extras == "yes":
            listaC += ',flamfull'+str(j)+'['+start[j-1] +':'+end[j-1]+']'
            for k in range (1,steps):
                step_dict['step'+str(k)] = ',flamstep'+str(k)+'_'+str(j)+'['+start[j-1] +':'+end[j-1]+']'
    else:
        listaB += ',final'+str(j)+'['+start[j-1] +':'+end[j-1]+']'
        if extras == "yes":
            listaC += ',flamfull'+str(j)+'['+start[j-1] +':'+end[j-1]+']'
            for k in range (1,steps):
                step_dict['step'+str(k)] += ',flamstep'+str(k)+'_'+str(j)+'['+start[j-1] +':'+end[j-1]+']'

    j = int(j) +1

#for each extraction, combine the orders into a single spectrum and write out fits and text files

#the "standard" extraction
#used to simply use odcombine to combine the orders, but it turns out that odcombine resamples to a linear dispersion function, oversampling some of the spectrum
#to avoid that, use the joinorders_noresampling routine (by Daniel Duschel Rutra, UFRGS) to join the orders
#joinorders_noresampling uses output from odcombine to find wavlength limits of each order, so we still need to run odcombine 
#after all that we put the XDGNIRS version into the header
#if you'd prefer to simply use odcombine:
#- change the 'output' field in the call to odcombine to '../PRODUCTS/'+target_name+'.fits'
#-Comment out the call to joinorders_noresampling [done]
#-uncomment the call to wspectext [done]

linear = sys.argv[9:10][0]
iraf.onedspec.interp='linear'


if linear == "yes":
    iraf.odcombine(listaB,output='PRODUCTS/'+target_name+'.fits',headers='',bpmasks='',rejmasks='',nrejmasks='',expmasks='',sigmas='',logfile='STDOUT',apertures='',group='all',first='no',w1='INDEF',w2='INDEF',dw='INDEF',nw='INDEF',log='no',combine='average',reject='none',outtype='real',outlimits='',smaskformat='bpmspectrum',smasktype='none',smaskvalue=0.0,blank=0.0,scale='none',zero='none',weight='none',statsec='',expname='',lthreshold='INDEF',hthreshold='INDEF',nlow=1,nhigh=1,nkeep=1,mclip='yes',lsigma=3.0,hsigma=3.0,rdnoise='0.0',gain='1.0',snoise='0.0',sigscale=0.1,pclip=-0.5,grow=0.0,offsets='physical',masktype='none',maskvalue=0.0,mode='al')
    iraf.hedit (images='PRODUCTS/'+target_name, fields='XDG_VER',value=version,add='yes',addonly='yes',delete='no',verify='no',show='no',update='yes')
    iraf.wspectext(input='PRODUCTS/'+target_name,output='PRODUCTS/'+target_name+'.txt',header='no',wformat='',mode='al')

else:
    iraf.odcombine(listaB,output='odcombine_output.fits',headers='',bpmasks='',rejmasks='',nrejmasks='',expmasks='',sigmas='',logfile='STDOUT',apertures='',group='all',first='no',w1='INDEF',w2='INDEF',dw='INDEF',nw='INDEF',log='no',combine='average',reject='none',outtype='real',outlimits='',smaskformat='bpmspectrum',smasktype='none',smaskvalue=0.0,blank=0.0,scale='none',zero='none',weight='none',statsec='',expname='',lthreshold='INDEF',hthreshold='INDEF',nlow=1,nhigh=1,nkeep=1,mclip='yes',lsigma=3.0,hsigma=3.0,rdnoise='0.0',gain='1.0',snoise='0.0',sigscale=0.1,pclip=-0.5,grow=0.0,offsets='physical',masktype='none',maskvalue=0.0,mode='al')
    gnirs_unc.joinorders_noresampling(temp,'odcombine_output.fits','PRODUCTS/'+target_name+'.fits',writefits=True,snrlist=None)
    iraf.hedit (images='PRODUCTS/'+target_name, fields='XDG_VER',value=version,add='yes',addonly='yes',delete='no',verify='no',show='no',update='yes')

if extras == "yes":
    #full-slit
    iraf.odcombine(input=listaC,output='odcombine_output_fullslit.fits',headers='',bpmasks='',rejmasks='',nrejmasks='',expmasks='',sigmas='',logfile='STDOUT',apertures='',group='all',first='no',w1='INDEF',w2='INDEF',dw='INDEF',nw='INDEF',log='no',combine='average',reject='none',outtype='real',outlimits='',smaskformat='bpmspectrum',smasktype='none',smaskvalue=0.0,blank=0.0,scale='none',zero='none',weight='none',statsec='',expname='',lthreshold='INDEF',hthreshold='INDEF',nlow=1,nhigh=1,nkeep=1,mclip='yes',lsigma=3.0,hsigma=3.0,rdnoise='0.0',gain='1.0',snoise='0.0',sigscale=0.1,pclip=-0.5,grow=0.0,offsets='physical',masktype='none',maskvalue=0.0,mode='al')
    gnirs_unc.joinorders_noresampling(temp,'odcombine_output_fullslit.fits','../PRODUCTS/'+target_name+'_fullslit.fits',writefits=True,snrlist=None)
    #iraf.wspectext(input='../PRODUCTS/'+target_name+'_fullslit',output='../PRODUCTS/'+target_name+'_fullslit.txt',header='no',wformat='',mode='al')
    iraf.hedit (images='../PRODUCTS/'+target_name+'_fullslit', fields='XDG_VER',value=version,add='yes',addonly='yes',delete='no',verify='no',show='no',update='yes')
    #step-by-step
    for k in range (1,steps):
        iraf.odcombine(input=step_dict['step'+str(k)],output='odcombine_output_step'+str(k),headers='',bpmasks='',rejmasks='',nrejmasks='',expmasks='',sigmas='',logfile='STDOUT',apertures='',group='all',first='no',w1='INDEF',w2='INDEF',dw='INDEF',nw='INDEF',log='no',combine='average',reject='none',outtype='real',outlimits='',smaskformat='bpmspectrum',smasktype='none',smaskvalue=0.0,blank=0.0,scale='none',zero='none',weight='none',statsec='',expname='',lthreshold='INDEF',hthreshold='INDEF',nlow=1,nhigh=1,nkeep=1,mclip='yes',lsigma=3.0,hsigma=3.0,rdnoise='0.0',gain='1.0',snoise='0.0',sigscale=0.1,pclip=-0.5,grow=0.0,offsets='physical',masktype='none',maskvalue=0.0,mode='al')
        gnirs_unc.joinorders_noresampling(temp,'odcombine_output_step'+str(k)+'.fits','../PRODUCTS/'+target_name+'_step'+str(k)+'.fits',writefits=True,snrlist=None)
        #iraf.wspectext(input='../PRODUCTS/'+target_name+'_step'+str(k), output='../PRODUCTS/'+target_name+'_step'+str(k)+'.txt',header='no',wformat='',mode='al')
        iraf.hedit (images='../PRODUCTS/'+target_name+'_step'+str(k), fields='XDG_VER',value=version,add='yes',addonly='yes',delete='no',verify='no',show='no',update='yes')

#sky spectrum, if needed for making the error spectrum
if error_spectrum == 'yes':
    for j in range (1,7):
        if j == 1:
            skylist = 'vsky_comb[sci,'+str(j)+']['+start[j-1] +':'+end[j-1]+']'
        else:
            skylist += ',vsky_comb[sci,'+str(j)+']['+start[j-1] +':'+end[j-1]+']'
        j = int(j) +1
    iraf.odcombine(skylist,output='odcombine_sky',headers='',bpmasks='',rejmasks='',nrejmasks='',expmasks='',sigmas='',logfile='STDOUT',apertures='',group='all',first='no',w1='INDEF',w2='INDEF',dw='INDEF',nw='INDEF',log='no',combine='average',reject='none',outtype='real',outlimits='',smaskformat='bpmspectrum',smasktype='none',smaskvalue=0.0,blank=0.0,scale='none',zero='none',weight='none',statsec='',expname='',lthreshold='INDEF',hthreshold='INDEF',nlow=1,nhigh=1,nkeep=1,mclip='yes',lsigma=3.0,hsigma=3.0,rdnoise='0.0',gain='1.0',snoise='0.0',sigscale=0.1,pclip=-0.5,grow=0.0,offsets='physical',masktype='none',maskvalue=0.0,mode='al')
    gnirs_unc.joinorders_noresampling(temp,'odcombine_sky.fits','sky.fits',writefits=True,snrlist=None)
    #iraf.wspectext(input='sky',output='sky.txt',header='no',wformat='',mode='al')

    
#Shift the spectra to rest (if desired by user)
if doshift == "yes":
    #Standard extraction
    iraf.dopcor (input='../PRODUCTS/'+target_name, output='../PRODUCTS/'+target_name+'_rest', redshift=redshift, isvelocity='no', add='no', dispersion='yes', flux='no', factor='3.0', apertures='', verbose='no', mode='al')
    iraf.wspectext(input='../PRODUCTS/'+target_name+'_rest',output='../PRODUCTS/'+target_name+'_rest.txt',header='no',wformat='',mode='al')
    #Sky spectrum
    if error_spectrum == 'yes':
        iraf.dopcor (input='sky', output='sky_rest', redshift=redshift, isvelocity='no', add='no', dispersion='yes', flux='no', factor='3.0', apertures='', verbose='no', mode='al')
        iraf.wspectext(input='sky_rest',output='sky_rest.txt',header='no',wformat='',mode='al')
    if extras == "yes":
        #Full-slit extraction
        iraf.dopcor (input='../PRODUCTS/'+target_name+'_fullslit', output='../PRODUCTS/'+target_name+'_fullslit_rest', redshift=redshift, isvelocity='no', add='no', dispersion='yes', flux='no', factor='3.0', apertures='', verbose='no', mode='al')
        iraf.wspectext(input='../PRODUCTS/'+target_name+'_fullslit_rest',output='../PRODUCTS/'+target_name+'_fullslit_rest.txt',header='no',wformat='',mode='al')
        #Step-by-step extractions
        for k in range (1,steps):
            iraf.dopcor (input='../PRODUCTS/'+target_name+'_step'+str(k), output='../PRODUCTS/'+target_name+'_step'+str(k)+'_rest', redshift=redshift, isvelocity='no', add='no', dispersion='yes', flux='no', factor='3.0', apertures='', verbose='no', mode='al')
            iraf.wspectext(input='../PRODUCTS/'+target_name+'_step'+str(k)+'_rest', output='../PRODUCTS/'+target_name+'_step'+str(k)+'_rest.txt',header='no',wformat='',mode='al')

#Thinking it would be a good idea to plot the separate orders so the user can judge if there are any unacceptable offsets and edit the regions used for combining, if they like
#Requires writing out relevant files as text
for j in range (1,7):
    if os.path.isfile('final6.txt'):
        iraf.delete (files="final"+str(j)+'.txt',verify="no",Stdout="/dev/null") 
    iraf.wspectext(input="final"+str(j),output="final"+str(j)+'.txt',header='no',wformat='',mode='al')
    if extras == "yes":
        if os.path.isfile('flamfull6.txt'):
            iraf.delete (files="flamfull"+str(j)+'.txt',verify="no",Stdout="/dev/null") 
        iraf.wspectext(input="flamfull"+str(j),output="flamfull"+str(j)+'.txt',header='no',wformat='',mode='al')
        for k in range (1,steps):
            if os.path.isfile('flamstep'+str(steps-1)+'_6.txt'):
                iraf.delete (files='flamstep'+str(k)+'_'+str(j)+'.txt',verify="no",Stdout="/dev/null") 
            iraf.wspectext(input='flamstep'+str(k)+'_'+str(j),output='flamstep'+str(k)+'_'+str(j)+'.txt',header='no',wformat='',mode='al')
            
#This is a useful plot that users should look at
make_orders_fig ("final", "PRODUCTS/orders.pdf")
if extras == 'yes':
    make_orders_fig ("flamfull", "PRODUCTS/orders_fullslit.pdf")
    for k in range (1,steps):
        make_orders_fig ("flamstep"+str(k)+"_", "PRODUCTS/orders_step"+str(k)+".pdf")

file.close()
