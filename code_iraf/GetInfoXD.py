#!/usr/bin/env python
#
##
##	GettingInfoXD.py:   		
##
##
#
# 2015.09.22: (jmiller,mpohlen) Change stdgraph to stgkern in imexam
#
# Import libraries
#

import sys
from pyraf import *
import astropy.io.fits
import numpy
from astropy.io.fits import getdata, getheader
import os
startdir = os.getcwd()
#iraf.reset(gemini="extern$gemini_public_1_11_1/")
iraf.gemini(_doprint=0, motd="no")
iraf.gnirs(_doprint=0)
iraf.imutil(_doprint=0)
iraf.onedspec(_doprint=0)
iraf.nsheaders('gnirs',Stdout='/dev/null')

def imexam(datfile, logfile, cenfile):
    #Find x location of spectrum in database/aperture file
    f = open(cenfile, 'r')
    for line in f:
        if 'center' in line:
            peak = line.split()[1]
            break
    #Write it to cursor file for imexam to use (and set y=340)
    cursorFile = open('curfile.txt', 'w')
    cursorFile.write(str(peak)+'  340 \n')
    cursorFile.close() 
    #Run imexam to find k-band counts and FWHM   
    iraf.unlearn(iraf.jimexam)
    iraf.imexam(input=datfile, frame=1, logfile=logfile, keeplog='yes', defkey='j', imagecur='curfile.txt', graphics ='stgkern', use_display='no',Stdout=1)

#Measure counts and FWHM
imexam ('target_comb[sci,1]', 'target.log', 'database/aptarget_comb_SCI_1_')    
imexam ('standard_comb[sci,1]', 'standard.log', 'database/apstandard_comb_SCI_1_')     

#Write galaxy header info into a file for WriteDataSheetXD.py
wdelt = iraf.hselect('target_comb[0]', field='OBJECT', expr='yes', Stdout=1)
wdelt = str(wdelt[0]).replace(" ","").replace('"','')
wdelt2 = iraf.hselect('target_comb[0]', field='GEMPRGID,AIRMASS,HA,PA,RAWIQ,RAWCC,RAWWV,RAWBG,AZIMUTH,DEC,DATE-OBS', expr='yes', Stdout=1)
wdelt2 = str(wdelt2[0])
wdelt2 = wdelt2.replace('\t',' ')
f = open('target_info.txt', 'w')
f.write(wdelt+" ")
f.write(wdelt2)
f.close()

#Same for standard star
wdelt = iraf.hselect('standard_comb[0]', field='OBJECT', expr='yes', Stdout=1)
wdelt = str(wdelt[0]).replace(" ","").replace('"','')
wdelt2 = iraf.hselect('standard_comb[0]', field='GEMPRGID,AIRMASS,HA,PA,RAWIQ,RAWCC,RAWWV,RAWBG,AZIMUTH,DEC,DATE-OBS', expr='yes', Stdout=1)
wdelt2 = str(wdelt2[0])
wdelt2 = wdelt2.replace('\t',' ')
f = open('standard_info.txt', 'w')
f.write(wdelt+" ")
f.write(wdelt2)
f.close()

#Fit continuum to order 3 (ext 1), divide, then measure S/N at 21000:24000 A
iraf.continuum('ftell_nolines1',output='stardiv1',type="ratio", func='spline3',sample='21000:24000', order=3, low_rej=2,high_rej=3,niterate=2,inter='no')
iraf.continuum('flam1',output='galdiv1',type="ratio", func='leg',sample='20000:23000', order=3, low_rej=2,high_rej=3,niterate=2,inter='no')

iraf.bplot(images="galdiv1",apertures="",band=1,graphics="stgkern",cursor=startdir+"/INTERMEDIATE/bplotcur.txt",next_image="",new_image="",overwrite="no",spec2="",constant=0.0,wavelength=0.0, linelist="",wstart=0.0,wend=0.0,dw=0.0,boxsize=2,Stdout="snrtgt.txt",StdoutG="dev$null") 
iraf.bplot(images="stardiv1",apertures="",band=1,graphics="stgkern",cursor=startdir+"/INTERMEDIATE/bplotcur.txt",next_image="",new_image="",overwrite="no",spec2="",constant=0.0,wavelength=0.0, linelist="",wstart=0.0,wend=0.0,dw=0.0,boxsize=2,Stdout='snrstd.txt',StdoutG="dev$null")

