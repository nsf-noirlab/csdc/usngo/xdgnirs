#!/usr/bin/env pyraf_python
#
##
##
##    SdistortAndWLcalibXD.py:           
##
##
#
# Import libraries
#


import sys
from pyraf import *
#iraf.reset(gemini="extern$gemini_public_1_11_1/")
iraf.gemini(_doprint=0, motd="no")
iraf.gnirs(_doprint=0)
iraf.imutil(_doprint=0)
iraf.nsheaders('gnirs',Stdout='/dev/null')

stdList = sys.argv[1:2][0]
tgtList = sys.argv[2:3][0]
doErrors = sys.argv[3:4][0]
skyList = sys.argv[4:5][0]

def combine_files(objlist, xc, outfile):
    files = []
    objfiles = open(objlist,'r')
    for line in objfiles.readlines():
        line = line.replace('\n','')
        files.append('ttfrlnc'+line)
    objString = ",".join(files)
    objfiles.close()
    iraf.nscombine(inimages=objString,output=outfile,masktype="none",fl_vardq='yes',rejtype="none",tolerance = 0.5,bpm = "",dispaxis = 1,pixscale = 1.,fl_cross = xc,fl_keepshift = 'no',fl_shiftint = 'yes',interptype = "linear",boundary = "nearest",constant = 0.,combtype = "average",maskvalue = 0.,statsec = "[*,*]",scale = "none",zero = "none",weight = "none",lthreshold = "INDEF", hthreshold = "INDEF",nlow = 1,  nhigh = 1,nkeep = 0,mclip = 'yes',lsigma = 5., hsigma = 5.,ron = 0.0, gain = 1.0,snoise = "0.0",sigscale = 0.1,pclip = -0.5,grow = 0.0,nrejfile = '',fl_inter = 'no',logfile = "",verbose = 'yes', debug = 'no',force = 'no')

def check_shifts(whichfile, xc):
    #There is currently (2015) a bug in nscombine that means it miscalculates the shifts at certain position angles
    #Here we check whether the shifts used by nscombine were correct (unfortunately I don't think we can give a shift to nscombine directly)

    ####
    #TEST
    #For my computer, I think that the '..' is unnecessary. I am going to remove it and try again.
    nodfile = open('LISTS/nod_to_sky.txt')
    ####
    #nodfile = open('../LISTS/nod_to_sky.txt')
    nodsize = nodfile.readline().split()[7]
    nodpix = float(nodsize) / -0.15
    nodfile.close()
    shifts = iraf.hselect(images=whichfile,fields='NSCHLX*',expr='yes',missing='INDEF',Stdout=1)
    shifts = [x.replace('\t', ',') for x in shifts]
    OK = True
    if xc == 'yes':
        message1 = "***WARNING: " 
        message2 = "It looks like the target wasn't bright enough to use nscombine.fl_cross+. Rerunning nscombine with fl_cross-."
    else:
        message1 = "ERROR: " 
        message2 = "Already tried using nscombine.fl_cross+. This looks like the nscombine bug, and XDGNIRS can't fix that."
    for x in shifts[0].split(','):
        if x != '0.00' and abs(float(x) - nodpix) > 1.5:
            print( message1, "shift of ", x, " pix in ", whichfile, " (NSCHLX* keyword) doesn't match expected value of ", nodpix, " pix. ", message2)
            OK = False
            break
        else:
            print( "***CHECK: Shift of ", x, " pix in ", whichfile, " (NSCHLX* keyword) is zero or close to expected value of ", nodpix, " pix. Looks OK.")
            continue
    return OK    
   
#Standard star. Should always be bright enough to use fl_cross+  
combine_files(stdList, 'yes', 'standard_comb.fits')

#Science target. First, try nscombine with fl_cross+
#If the target is bright enough, this should avoid the bug where nscombine gets the wrong shifts from the headers at certain PAs
combine_files(tgtList, 'yes', 'target_comb.fits')  
#Check that the shift is sensible
shiftOK = check_shifts('target_comb[0]', 'yes')
#If not, try again with fl_cross-
if not shiftOK:
    iraf.imdelete('target_comb')
    combine_files(tgtList, 'no', 'target_comb.fits') 
    shiftOK = check_shifts('target_comb[0]', 'yes')

#If user has requested an error spectrum, also combine the sky files
if doErrors == 'yes':
    combine_files(skyList, 'no', 'sky_comb.fits')  



