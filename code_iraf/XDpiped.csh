#!/usr/bin/bash
#

##########################################################################################################" 
##	Pipeline for data reduction of GNIRS cross-dispersed data        				##"
##													##"
##	Original version: O. Gonzalez Martin (Instituto de Astrofisica de Canarias, Tenerife, Spain)	##"
##	Staring date: Monday 22th of February 2012							##"
##	Finished date: v.0.  7th of April 2012	(run with NGC3079)				        ##"
##      Extended and generalised by R. Mason #Comment this if statement out to test the new IRAF.app
######(Gemini Observatory),  2012 - 2016                         ##"
##                                                                                                      ##"
##########################################################################################################" 

set startdir = `pwd`
echo $startdir

#XDGNIRS currently accepts the input data list plus a number of other options
#The options are described below
set infile = $1
set entry1 = $2
set entry2 = $3
set entry3 = $4
set entry4 = $5
set entry5 = $6
set entry6 = $7
set entry7 = $8
set entry8 = $9
set entry9 = $10
set entry10 = $11
set entry11 = $12
set entry12 = $13
set entry13 = $14
set entry14 = $15
set entry15 = $16
set entry16 = $17
set entry17 = $18
set entry18 = $19
set entry19 = $20
set entry20 = $21
set entry21 = $22
set entry22 = $23
set entry23 = $24
set entry24 = $25
set entry25 = $26
set entry26 = $27
set entry27 = $28
set entry28 = $29
set entry29 = $30
set entry30 = $31
set entry31 = $32
set entry32 = $33

#Typing xdgnirs -h brings up this help
if ($infile == "-h") then
    echo " "
    echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
    echo  "To run the code:"
    echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
    echo ""
    echo "ls *fits > inputfiles.lst"
        echo "xdpiped inputfiles.lst"
        echo "or"
    echo "xdpiped inputfiles.lst gemiraf_ver=1.11.1"
    echo " "
    echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
    echo  "The optional keywords are:"
    echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
    echo " "
    echo "rawdir: (./)  [location of raw data]"
    echo "step: 0-13 (0)  [step at which to start the reduction]"
    echo "stop: 0-13 (13)  [step after which to stop the reduction]"
    echo "cleanir_sciflag: nfrq (f) [whether/how to run cleanir.py on the science target spectra]"
    echo "cleanir_stdflag: nfrq (f) [whether/how to run cleanir.py on the standard star spectra]"
    echo "cleanir_arcflag: nfrq (n) [whether/how to run cleanir.py on the arc spectra]"
    echo "cleanir_IRflag: nfrq (n) [whether/how to run cleanir.py on the IR flat spectra]"
    echo "cleanir_QHflag: nfrq (n) [whether/how to run cleanir.py on the QH flat spectra]"
    echo "cleanir_pinflag: nfrq (n) [whether/how to run cleanir.py on the pinhole flat spectra]"
    echo "nsflat_inter: yes/no (no) [run nsflat interactively?]"
    echo "despike_tgt: yes/no (yes) [attempt to remove spikes from science target data?]"
    echo "despike_std: yes/no (no) [attempt to remove spikes from standard star data?]"
    echo "tgt_thresh: (20) [threshold for removing spikes from science target data]"
    echo "std_thresh: (50) [threshold for removing spikes from standard star data]"
    echo "nssdist_inter: yes/no (no) [run nssdist interactively?]"
    echo "nswav_inter: yes/no (no) [run nswavelength interactively?]"
    echo "nsfit_inter: yes/no (no) [run nsfitcoords interactively?]" 
    echo "nsext_inter: yes/no (no) [run nsextract interactively?]"
    echo "use_apall: yes/no (no) [use apall to do a weighted extraction?]"
    echo "aperture: 1-40 (6) [extraction aperture, pixels (+/-)]"
    echo "extras: yes/no (no) [extract full-slit spectrum and in steps along the slit?]"
    echo "columns: 1-40 (6) [width of each extraction step along the slit (pixels)]"
    echo "hlines: vega/linefit_auto/linefit_manual/vega_tweak/linefit_tweak/none (linefit_auto) [technique for removing H lines from standard star]"
    echo "telluric1_inter: yes/no (no) [run 'telluric' interactively when using vega_tweak option?]"
    echo "telluric2_inter: yes/no (no) [run 'telluric' interactively when dividing by standard star?]"
    echo "continuum_inter: yes/no (no) [interactive continuum fitting?]"
    echo "offsets: no/manual (no) [enter specplot, edit order scaling?]"
    echo "shift_to_rest: yes/no (no) [look up redshift and shift to rest frame?]"
    echo "error_spectrum: yes/no (no) [generate S/N and error spectra? Only works on data nodded off the slit]"
    #echo "gemiraf_ver: 1.11.1/1.12+ (1.12+) [User's Gemini IRAF version]"
    echo " "
    echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
    echo  "The steps are:"
    echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
    echo " "
    echo "0: Identify files and create lists"
    echo "1: Clean the data with cleanir.py"
    echo "2: Prepare the data (with nsprepare) and measure some statistics"
    echo "3: Make the flatfield"
    echo "4: Reduce the science target and standard star data (de-spike, flatfield, and sky subtract)"
    echo "5: Rectify and wavelength-calibrate the data"
    echo "6: Combine the files"
    echo "7: Extract spectra"
    echo "8: Remove the H lines in the standard star"
    echo "9: Perform the telluric line cancellation"
    echo "10: Flux calibrate the science target spectrum"
    echo "11: Combine the orders"
    echo "12: Create an error spectrum"
    echo "13: Create an informational PDF file"
    echo " "
    echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
    exit
endif

#Check that the input file exists
set infilen = `echo $infile | cut -f1 -d"."`
if (! -e ${infile}) then
    echo "Can't find your input list file," ${infile}". Exiting script."
    exit
endif

#Create the relevant directories
(mkdir LISTS)  >& /dev/null
(mkdir INTERMEDIATE)  >& /dev/null
(mkdir PRODUCTS)  >& /dev/null

(ln -sf ${HOME}/login.cl LISTS/.)  >& /dev/null
(ln -sf ${HOME}/login.cl .)  >& /dev/null

#Definitions:
set timenow = `date +%F" "%T`
set timename = `date +%F"_"%T`
#set startdir = `pwd`
set version = 'XDGNIRS v2.2.6'
set date = `date`
set procdir = ${XDGNIRSDIR}
set timenow = `date +%F" "%T`


#Write the command-line options to a file that will be used by InitOptionXD.csh
echo $entry1 >!  ${startdir}/LISTS/entries.txt
echo $entry2 >>  ${startdir}/LISTS/entries.txt
echo $entry3 >>  ${startdir}/LISTS/entries.txt
echo $entry4 >>  ${startdir}/LISTS/entries.txt
echo $entry5 >>  ${startdir}/LISTS/entries.txt
echo $entry6 >>  ${startdir}/LISTS/entries.txt
echo $entry7 >>  ${startdir}/LISTS/entries.txt
echo $entry8 >>  ${startdir}/LISTS/entries.txt
echo $entry9 >>  ${startdir}/LISTS/entries.txt
echo $entry10 >>  ${startdir}/LISTS/entries.txt
echo $entry11 >>  ${startdir}/LISTS/entries.txt
echo $entry12 >>  ${startdir}/LISTS/entries.txt
echo $entry13 >>  ${startdir}/LISTS/entries.txt
echo $entry14 >>  ${startdir}/LISTS/entries.txt
echo $entry15 >>  ${startdir}/LISTS/entries.txt
echo $entry16 >>  ${startdir}/LISTS/entries.txt
echo $entry17 >>  ${startdir}/LISTS/entries.txt
echo $entry18 >>  ${startdir}/LISTS/entries.txt
echo $entry19 >>  ${startdir}/LISTS/entries.txt
echo $entry20 >>  ${startdir}/LISTS/entries.txt
echo $entry21 >>  ${startdir}/LISTS/entries.txt
echo $entry22 >>  ${startdir}/LISTS/entries.txt
echo $entry23 >>  ${startdir}/LISTS/entries.txt
echo $entry24 >>  ${startdir}/LISTS/entries.txt
echo $entry25 >>  ${startdir}/LISTS/entries.txt
echo $entry26 >>  ${startdir}/LISTS/entries.txt
echo $entry27 >>  ${startdir}/LISTS/entries.txt
echo $entry28 >>  ${startdir}/LISTS/entries.txt
echo $entry29 >>  ${startdir}/LISTS/entries.txt
echo $entry30 >>  ${startdir}/LISTS/entries.txt
echo $entry31 >>  ${startdir}/LISTS/entries.txt
echo $entry32 >>  ${startdir}/LISTS/entries.txt

#Use InitOptionXD.csh to defined variables containing the command-line options (or to set them to default values)
set entry_point = `csh ${procdir}/InitOptionXD.csh "step" ${startdir}`
set exit_point = `csh ${procdir}/InitOptionXD.csh "stop" ${startdir}`
set nsflat_inter = `csh ${procdir}/InitOptionXD.csh "nsflat_inter" ${startdir}`
set despike_tgt = `csh ${procdir}/InitOptionXD.csh "despike_tgt" ${startdir}`
set despike_std = `csh ${procdir}/InitOptionXD.csh "despike_std" ${startdir}`
set tgt_thresh = `csh ${procdir}/InitOptionXD.csh "tgt_thresh" ${startdir}`
set std_thresh = `csh ${procdir}/InitOptionXD.csh "std_thresh" ${startdir}`
set nssdist_inter = `csh ${procdir}/InitOptionXD.csh "nssdist_inter" ${startdir}`
set nswav_inter = `csh ${procdir}/InitOptionXD.csh "nswav_inter" ${startdir}`
set nsfit_inter = `csh ${procdir}/InitOptionXD.csh "nsfit_inter" ${startdir}`
set nsext_inter = `csh ${procdir}/InitOptionXD.csh "nsext_inter" ${startdir}`
set continuum_inter = `csh ${procdir}/InitOptionXD.csh "continuum_inter" ${startdir}`
set telluric1_inter = `csh ${procdir}/InitOptionXD.csh "telluric1_inter" ${startdir}`
set telluric2_inter = `csh ${procdir}/InitOptionXD.csh "telluric2_inter" ${startdir}`
set cleanir_sciflag = `csh ${procdir}/InitOptionXD.csh "cleanir_sciflag" ${startdir}`
set cleanir_stdflag = `csh ${procdir}/InitOptionXD.csh "cleanir_stdflag" ${startdir}`
set cleanir_arcflag = `csh ${procdir}/InitOptionXD.csh "cleanir_arcflag" ${startdir}`
set cleanir_IRflag = `csh ${procdir}/InitOptionXD.csh "cleanir_IRflag" ${startdir}`
set cleanir_QHflag = `csh ${procdir}/InitOptionXD.csh "cleanir_QHflag" ${startdir}`
set cleanir_pinflag = `csh ${procdir}/InitOptionXD.csh "cleanir_pinflag" ${startdir}`
set use_apall = `csh ${procdir}/InitOptionXD.csh "use_apall" ${startdir}`
set aperture = `csh ${procdir}/InitOptionXD.csh "aperture" ${startdir}`
set extras = `csh ${procdir}/InitOptionXD.csh "extras" ${startdir}`
set stepsize = `csh ${procdir}/InitOptionXD.csh "columns" ${startdir}`
set hlines = `csh ${procdir}/InitOptionXD.csh "hlines" ${startdir}`
set deredshift = `csh ${procdir}/InitOptionXD.csh "shift_to_rest" ${startdir}`
set offsets = `csh ${procdir}/InitOptionXD.csh "offsets" ${startdir}`
set rawdir = `csh ${procdir}/InitOptionXD.csh "rawdir" ${startdir}`
set error_spec = `csh ${procdir}/InitOptionXD.csh "error_spectrum" ${startdir}`
set gemiraf = `csh ${procdir}/InitOptionXD.csh "gemiraf_ver" ${startdir}`
set wave_soln_linear = `csh ${procdir}/InitOptionXD.csh "wave_soln_linear" ${startdir}`

#If reduction is starting from scratch, make new XDGNIRS_Log and Checks_and_Warnings files
#All the output from the code is copied to the XDGNIRS_Log file
#Any output lines containing "CHECK" or "WARNING" are copied to the Checks_and_Warnings file at the end of the reduction
#Lines labelled "CHECK" indicate that checks have been performed and passed
#These aren't definitive checks; thresholds etc. may need to be refined as experience is gained with the data
if (${entry_point} == 0) then
    (rm -f ${startdir}/PRODUCTS/Checks_and_Warnings.txt) >& /dev/null
    touch ${startdir}/PRODUCTS/Checks_and_Warnings.txt
    (rm -f ${startdir}/PRODUCTS/XDGNIRS_Log.txt) >& /dev/null
    touch ${startdir}/PRODUCTS/XDGNIRS_Log.txt
endif

echo "" | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt ${startdir}/PRODUCTS/Checks_and_Warnings.txt
echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@" | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt ${startdir}/PRODUCTS/Checks_and_Warnings.txt
echo " Processing list of files "${infile}" with "${version}": Started at "${timenow} | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt ${startdir}/PRODUCTS/Checks_and_Warnings.txt

#Check that the keywords are all recognised keywords, and exit if they aren't
sed '/^$/d' ${startdir}/LISTS/entries.txt > ${startdir}/LISTS/temp.txt
set n_options = `wc -l ${startdir}/LISTS/temp.txt | awk '{print $1}'`
rm ${startdir}/LISTS/temp.txt >& /dev/null
pyraf_python ${procdir}/CheckKeywords.py $n_options $2 $3 $4 $5 $6 $7 $8 $9 $10 $11 $12 $13 $14 $15 $16 $17 $18 $19 $20 $21 $22 $23 $24 $25 $26 $27 $28 $29 $30 $31 $32 | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt

#After every step we check the log file for errors 
#We only want to find errors that have occurred after the most recent processing started, ignoring old ones
#set error_check = `tail -r ${startdir}/PRODUCTS/XDGNIRS_Log.txt | grep 'Processing' -m 1 -B 9999 | tail -r | grep 'ERROR'`
#That gives broken pipe error; try something complicated instead...
set temp = `grep 'Processing' ${startdir}/PRODUCTS/XDGNIRS_Log.txt | tail -n 1`
sed -n -e "/$temp/"',$p' ${startdir}/PRODUCTS/XDGNIRS_Log.txt > error_record.txt
set error_check = `grep 'ERROR' error_record.txt`
if("$error_check" != "") then
        goto clean_exit
endif

#Check that the input parameters are sensible, and exit if they aren't
pyraf_python ${procdir}/CheckOptions.py $entry_point $exit_point $nsflat_inter $despike_tgt $despike_std $tgt_thresh $std_thresh $nssdist_inter $nswav_inter $nsfit_inter $nsext_inter $continuum_inter $telluric1_inter $cleanir_sciflag $cleanir_stdflag $cleanir_arcflag $cleanir_IRflag $cleanir_QHflag $cleanir_pinflag $use_apall $aperture $extras $stepsize $hlines $deredshift $rawdir $gemiraf $offsets $error_spec $telluric2_inter $wave_soln_linear |tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
set temp = `grep 'Processing' ${startdir}/PRODUCTS/XDGNIRS_Log.txt | tail -n 1`
sed -n -e "/$temp/"',$p' ${startdir}/PRODUCTS/XDGNIRS_Log.txt > error_record.txt
set error_check = `grep 'ERROR' error_record.txt`
if("$error_check" != "") then
        goto clean_exit
endif

#Record all the options in the XDGNIRS_Log file
echo " Options: rawdir="$rawdir "start="$entry_point "stop="$exit_point "nsflat_inter="$nsflat_inter "despike_tgt"=$despike_tgt "despike_std"=$despike_std "tgt_thresh="$tgt_thresh "std_thresh="$std_thresh "nssdist_inter="$nssdist_inter "nswav_inter="$nswav_inter "nsfit_inter="$nsfit_inter "nsext="$nsext_inter "continuum_inter="$continuum_inter "telluric1_inter="$telluric1_inter "telluric2_inter="$telluric2_inter "cleanir_sciflag="$cleanir_sciflag "cleanir_stdflag="$cleanir_stdflag "cleanir_arcflag="$cleanir_arcflag "cleanir_IRflag="$cleanir_IRflag "cleanir_QHflag="$cleanir_QHflag "cleanir_pinflag="$cleanir_pinflag "use_apall="$use_apall "aperture="$aperture "extras="$extras "columns="$stepsize "hlines="$hlines "offsets"=$offsets "shift_to_rest="$deredshift "error_spectrum="$error_spec "gemiraf_ver="$gemiraf "wave_soln_linear="$wave_soln_linear | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt

#Set up links to files in raw data directory
set rawfiles = `cat $infile`
if ("$rawdir" != "./") then
    foreach file (${rawfiles})
    ln -sf ${rawdir}/${file} .
    end
endif

##
#### PROCESSING STEPS:
##

if (${entry_point} == 0) then
     goto loop0
else if (${entry_point} == 1) then
     goto loop1
else if (${entry_point} == 2) then
     goto loop2
else if (${entry_point} == 3) then
     goto loop3
else if (${entry_point} == 4) then
     goto loop4
else if (${entry_point} == 5) then
     goto loop5
else if (${entry_point} == 6) then
     goto loop6
else if (${entry_point} == 7) then
     goto loop7
else if (${entry_point} == 8) then
     goto loop8
else if (${entry_point} == 9) then
     goto loop9
else if (${entry_point} == 10) then
     goto loop10
else if (${entry_point} == 11) then
     goto loop11
else if (${entry_point} == 12 && $error_spec == 'no') then
     echo "ERROR"
     echo "Entry point: "${entry_point}" is the error spectrum generation, but you have specified error_spectrum=no. Exiting script."
     exit
else if (${entry_point} == 12 && $error_spec == 'yes') then
     goto loop12
else if (${entry_point} == 13) then
     goto loop13
else
     echo "ERROR"
     echo "Entry point: "${entry_point}" not supported. Exiting script. Type xdgnirs -h to see allowed values."
     exit
endif

#Most of these loops follow the same basic format, more-or-less:
#- remove some existing files, if necessary
#- cd to the INTERMEDIATE directory
#- call one or more pieces of code to do something
#- check for errors and exit cleanly if any are found
#- if the user said to exit after this step, do so
#- otherwise, go back to the starting directory
#A few of the loops call extra pieces of code (e.g. to look up magnitudes or redshifts)
#Some of them also read variables from files, or create them from information therein


loop0:

echo ""
echo "*************************************" | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
echo "**   Identification of Files       **" | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
echo "*************************************" | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt

#See what's in the input files, and exit if daft things like non-GNIRS files are found
#pyraf_python ${procdir}/ObsidentifyXD.py $infile $infilen  | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
pyraf_python ${procdir}/ObsidentifyXD.py $infile $infilen  | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt

set temp = `grep 'Processing' ${startdir}/PRODUCTS/XDGNIRS_Log.txt | tail -n 1`
sed -n -e "/$temp/"',$p' ${startdir}/PRODUCTS/XDGNIRS_Log.txt > error_record.txt
set error_check = `grep 'ERROR' error_record.txt`
if("$error_check" != "") then
        goto clean_exit
endif

echo "*************************************" | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
echo "**         Creating Lists          **" | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
echo "*************************************" | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt

(rm -f ${startdir}/LISTS/qoffsets.txt)  >& /dev/null

#Make all the necessary lists. These are written into the LISTS directory
csh ${procdir}/mklistXD.csh ${infilen} ${startdir} ${procdir}

#At this point we also figure out what the Q offsets are and whether the sequence nodded to sky
#FindSpectra.py explains a bit more about why that information is needed
#Check that the Texp/coadds didn't change, either
pyraf_python ${procdir}FindSpectra.py ${startdir}/LISTS/acqtarget.lst ${startdir}/LISTS/acqstd.lst ${startdir}/LISTS/target.lst ${startdir}/LISTS/standard.lst ${startdir}/LISTS/targetA.lst ${startdir}/LISTS/targetB.lst ${startdir}/LISTS/temp ${startdir}/LISTS/qoffsets.txt | tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt

#And, of course, check whether any errors were raised
set temp = `grep 'Processing' ${startdir}/PRODUCTS/XDGNIRS_Log.txt | tail -n 1`
sed -n -e "/$temp/"',$p' ${startdir}/PRODUCTS/XDGNIRS_Log.txt > error_record.txt
set error_check = `grep 'Error\|ERROR' error_record.txt`
if("$error_check" != "") then
        goto clean_exit
endif

if (${exit_point} == 0) then
    goto user_exit
endif


loop1: 

echo "*************************************" >> ${startdir}/PRODUCTS/XDGNIRS_Log.txt
echo "**  Cleaning the Data Set          **" >> ${startdir}/PRODUCTS/XDGNIRS_Log.txt
echo "*************************************" >> ${startdir}/PRODUCTS/XDGNIRS_Log.txt


(rm -f ${startdir}/INTERMEDIATE/cN*.fits) >& /dev/null
(rm -f ${startdir}/INTERMEDIATE/N*.fits) >& /dev/null
(ln -s ${startdir}/N*fits ${startdir}/INTERMEDIATE/.)
cd ${startdir}/INTERMEDIATE/

#Either run cleanir.py (according to the input options or defaults), or skip the cleaning and just create files with the same names
foreach list (target.lst:$cleanir_sciflag standard.lst:$cleanir_stdflag Arc.lst:$cleanir_arcflag IRhigh.lst:$cleanir_IRflag QH.lst:$cleanir_QHflag pinholes.lst:$cleanir_pinflag) 
    set obslist = `echo $list | cut -d ':' -f1`
    set flag = `echo $list | cut -d ':' -f2`
    set count=`wc -l ${startdir}/LISTS/$obslist | awk '{print $1}'`
    set i =  `expr 1`
    while ($i <= $count) 
	set filename = `cat  ${startdir}/LISTS/$obslist | head -${i} | tail -1 | awk '{print $1}'`
	    if (${flag} != 'n') then 
		pyraf_python ${procdir}/cleanir.py -${flag} ${filename} | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
	    else 
		#to simplify things later, (symbolic link to) file with "c" prefix is created even if cleaning not done
		ln -sf ${filename} c${filename}
	    endif
        set i =  `expr $i + 1`
    end
end

if (${exit_point} == 1) then
    goto user_exit
endif
cd ${startdir}


loop2: 

echo "*************************************" | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
echo "**  Preparing the Files            **" | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
echo "*************************************" | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt


(rm -f ${startdir}/INTERMEDIATE/ncN*.fits) >& /dev/null
(rm -f ${startdir}/INTERMEDIATE/rncN*.fits) >& /dev/null
(rm -f ${startdir}/INTERMEDIATE/newBPM.fits) >& /dev/null
ln -s ${procdir}/newBPM.fits ${startdir}/INTERMEDIATE/.

set nQH = `wc ${startdir}/LISTS/QH.lst | awk '{print $1}'`
set nIR = `wc ${startdir}/LISTS/IRhigh.lst  | awk '{print $1}'`

cd ${startdir}/INTERMEDIATE/

#Use StatsandPrepare.py to run nsprepare and record some statistics
#Process daytime pinholes and nighttime observations separately (because sometimes there are small shifts between them)

pyraf_python ${procdir}/StatsAndPrepareXD.py ${startdir}/LISTS/pinholes.lst n ${gemiraf} ${nQH} ${nIR} "no" ${procdir}config/ | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt

#Check for errors
#Nsprepare often gives harmless (I think?) "error" message about the WCS. Ignore those, only "Error" and "ERROR" signal real problems
set temp = `grep 'Processing' ${startdir}/PRODUCTS/XDGNIRS_Log.txt | tail -n 1`
sed -n -e "/$temp/"',$p' ${startdir}/PRODUCTS/XDGNIRS_Log.txt > error_record.txt
set error_check = `grep 'Error\|ERROR' error_record.txt`
if("$error_check" != "") then
        goto clean_exit
endif

pyraf_python ${procdir}/StatsAndPrepareXD.py ${startdir}/LISTS/NightObs.lst n ${gemiraf} ${nQH} ${nIR} "yes" ${procdir}config/ | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt

set temp = `grep 'Processing' ${startdir}/PRODUCTS/XDGNIRS_Log.txt | tail -n 1`
sed -n -e "/$temp/"',$p' ${startdir}/PRODUCTS/XDGNIRS_Log.txt > error_record.txt
set error_check = `grep 'Error\|ERROR' error_record.txt`
if("$error_check" != "") then
        goto clean_exit
endif
if (${exit_point} == 2) then
    goto user_exit
endif
cd ${startdir}


loop3: 

echo "*************************************" | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
echo "**      Making the FlatField       **" | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
echo "*************************************" | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt


(rm -f ${startdir}/LISTS/QH.fits) >& /dev/null
(rm -f ${startdir}/LISTS/IRhigh.fits) >& /dev/null
(rm -f ${startdir}/LISTS/IRhigh*.pl) >& /dev/null
(rm -f ${startdir}/LISTS/QH*.pl) >& /dev/null
(rm -f ${startdir}/INTERMEDIATE/MasterFlat.fits) >& /dev/null
cd ${startdir}/INTERMEDIATE/

#Use MakeFlatfieldXD.py to make a master flat out of the IR and QH flats
pyraf_python ${procdir}/MakeFlatfieldXD.py ${startdir}/LISTS/IRhigh.lst ${startdir}/LISTS/QH.lst ${nsflat_inter} ${procdir} | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt

#Sometimes nsflat fails with errors like "unsupported operand type(s) for /: '_INDEFClass' and '_INDEFClass'", or a fixpix floating point error
#Never figured out what was going on with that
#Flatfielding.py will rerun nsflat with different parameters to try to get round that problem, but errors will still be written to the status file
#Using grep -v 502\|NSFLAT ERROR will ignore those errors
#This is not a perfect solution;  may let some other errors fall through the cracks...
set temp = `grep 'Processing' ${startdir}/PRODUCTS/XDGNIRS_Log.txt | tail -n 1`
sed -n -e "/$temp/"',$p' ${startdir}/PRODUCTS/XDGNIRS_Log.txt > error_record.txt
set error_check = `grep 'Error\|ERROR' error_record.txt | grep -v '502\|NSFLAT ERROR'`
#If nothing else found, check for the message written if nsflat keeps failing and the script gives up
if("$error_check" == "") then
        set error_check = `grep 'exiting' ${startdir}/PRODUCTS/XDGNIRS_Log.txt`
endif
if("$error_check" != "") then
        goto clean_exit
endif
if (${exit_point} == 3) then
    goto user_exit
endif
cd ${startdir}


loop4: 

echo "******************************************" | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
echo "** Reducing the Science + Standard Data **" | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
echo "******************************************" | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt

cd ${startdir}/INTERMEDIATE/
(rm -f ${startdir}/INTERMEDIATE/lnc*.fits) >& /dev/null 
(rm -f ${startdir}/INTERMEDIATE/rlnc*.fits) >& /dev/null 

#At this point we want to remove "spikes" and flatfield and sky subtract the science target and standard star data
#GNIRS short camera data used to be full of spikes caused by radioactive lens coatings
#Since the lens replacement in 2012 they have had fewer spikes but there are still some and it's worth removing them in relatively long exposure data

#I had a go at using gemcrspec, the Gemini implementation of LACOSMIC
#It's not really intended for GNIRS and I didn't get it to work very well
#So the default is the old algorithm (see ReducingXD.py code), the LACOSMIC option is commented out
#Never got round to implementing this choice as an input option - WISHLIST

#User may have chosen not to remove spikes from science target
#And the default is not to bother for the standard star (don't always get good results and they have few spikes anyway)
#In those cases we use AltReducingXD.py just to flatfield and sky-subtract the un-cleaned files
if (${despike_tgt} == 'no') then
    set despike = 'no'
    pyraf_python ${procdir}/AltReducingXD.py ${startdir}/LISTS/target.lst ${despike} ${gemiraf} ${procdir} ${error_spec} | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt 
endif
if (${despike_std} == 'no') then
    set despike = 'no'
    pyraf_python ${procdir}/AltReducingXD.py ${startdir}/LISTS/standard.lst ${despike} ${gemiraf} ${procdir} ${error_spec} | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt 
endif

#======
#This section calls the old radiation event removal algorithm
if (${despike_tgt} == 'yes') then
    (rm -f ${startdir}/LISTS/targetB.pl) >& /dev/null 
    (rm -f ${startdir}/LISTS/targetA.pl) >& /dev/null 
    (rm -f ${startdir}/LISTS/targetB.fits) >& /dev/null 
    (rm -f ${startdir}/LISTS/targetA.fits) >& /dev/null
    pyraf_python ${procdir}/ReducingXD.py ${startdir}/LISTS/targetA.lst ${startdir}/LISTS/targetB.lst ${tgt_thresh} ${gemiraf} ${procdir} ${error_spec} | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt 
endif
if (${despike_std} == 'yes') then
    echo '****DESPIKING'
    (rm -f ${startdir}/LISTS/standardB.pl) >& /dev/null 
    (rm -f ${startdir}/LISTS/standardA.pl) >& /dev/null 
    (rm -f ${startdir}/LISTS/standardB.fits) >& /dev/null 
    (rm -f ${startdir}/LISTS/standardA.fits) >& /dev/null
    pyraf_python ${procdir}/ReducingXD.py ${startdir}/LISTS/standardA.lst ${startdir}/LISTS/standardB.lst ${std_thresh} ${gemiraf} ${procdir} ${error_spec} | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt 
endif

#=======
#Uncomment this section to use LACOSMIC/GEMCRSPEC
#
#if (${despike_tgt} == 'yes') then
    #set despike = 'yes'
    #pyraf_python ${procdir}/AltReducingXD.py ${startdir}/LISTS/target.lst ${despike} ${gemiraf} ${procdir} ${error_spec} | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt 
#endif
#if (${despike_std} == 'yes') then
    #set despike = 'yes'
    #pyraf_python ${procdir}/AltReducingXD.py ${startdir}/LISTS/standard.lst ${despike} ${gemiraf} ${procdir} ${error_spec} | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt 
#endif
#=======

#If there is already an NSFLAT error in the Status file from the flatfielding step, probably want to ignore it
set temp = `grep 'Processing' ${startdir}/PRODUCTS/XDGNIRS_Log.txt | tail -n 1`
sed -n -e "/$temp/"',$p' ${startdir}/PRODUCTS/XDGNIRS_Log.txt > error_record.txt
set error_check = `grep 'Error\|ERROR' error_record.txt | grep -v '502\|NSFLAT ERROR'`
if("$error_check" != "") then
        goto clean_exit
endif
if (${exit_point} == 4) then
    goto user_exit
endif
cd ${startdir}


loop5: 

echo "*************************************" | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
echo "**   S-Distortion Correction       **" | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
echo "**   Wavelength Calibration        **" | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
echo "*************************************" | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt

(rm -f ${startdir}/INTERMEDIATE/*arc_comb*.fits) >& /dev/null 
(rm -f ${startdir}/INTERMEDIATE/calibrated_arc.fits) >& /dev/null 
(rm -f ${startdir}/INTERMEDIATE/fr*.fits) >& /dev/null 
(rm -f ${startdir}/INTERMEDIATE/fk*.fits) >& /dev/null 
(rm -f ${startdir}/INTERMEDIATE/tfr*.fits) >& /dev/null 
(rm -f ${startdir}/INTERMEDIATE/tfk*.fits) >& /dev/null 
(rm -f ${startdir}/INTERMEDIATE/ttfr*.fits) >& /dev/null
(rm -f ${startdir}/INTERMEDIATE/ttfk*.fits) >& /dev/null 
(rm -rf ${startdir}/INTERMEDIATE/database) >& /dev/null 
(rm -f ${startdir}/INTERMEDIATE/nssdist.cl )  >& /dev/null 
ln -s ${procdir}/nssdist.cl ${startdir}/INTERMEDIATE/.
cd ${startdir}/INTERMEDIATE/

#Use SdistorAndWLcalibXD.py to rectify and wavelength-calibrate the science target and standard star data
#The qualty of the transformation is something I'd take a closer look at if I had time - WISHLIST
pyraf_python ${procdir}/SdistorAndWLcalibXD.py  ${startdir}/LISTS/pinholes.lst  ${startdir}/LISTS/Arc.lst  ${startdir}/LISTS/target.lst ${startdir}/LISTS/standard.lst ${nssdist_inter} ${nswav_inter} ${nsfit_inter} ${gemiraf} ${procdir}  | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt

set temp = `grep 'Processing' ${startdir}/PRODUCTS/XDGNIRS_Log.txt | tail -n 1`
sed -n -e "/$temp/"',$p' ${startdir}/PRODUCTS/XDGNIRS_Log.txt > error_record.txt
set error_check = `grep 'Error\|ERROR' error_record.txt | grep -v '502\|NSFLAT ERROR'`
#Also looks for failed wavelength calibration
set cal_fail = `grep 'no calibration\|no solution' ${startdir}/PRODUCTS/XDGNIRS_Log.txt`
echo $cal_fail
set error_check = `echo $error_check $cal_fail`
if("$error_check" != "") then
        goto clean_exit
endif
if (${exit_point} == 5) then
    goto user_exit
endif
cd ${startdir}


loop6: 

echo "*************************************" | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
echo "**   Combining Spectra             **" | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
echo "*************************************" | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt

(rm -f ${startdir}/INTERMEDIATE/*target_comb.fits) >& /dev/null 
(rm -f ${startdir}/INTERMEDIATE/*standard_comb.fits) >& /dev/null 
(rm -f ${startdir}/INTERMEDIATE/*sky_comb.fits) >& /dev/null
cd ${startdir}/INTERMEDIATE/

#Combine all the individual files for the science target and standard star

#First, find out whether science target observations nodded off the slit; if so, don't try to shift and combine B beam spectra with A beam ones
#Standard star observations assumed to nod along the slit (not off to sky)
set sky_check = `grep 'Observations nodded off slit' ${startdir}/LISTS/nod_to_sky.txt`
if ("$sky_check" == "") then
    set nscombine_files = ${startdir}/LISTS/target.lst
    #These needed for noise calculation - WON'T WORK PROPERLY ON DATA NODDED ALONG SLIT
    set sky_files = ${startdir}/LISTS/targetB.lst
    echo Science target observations nodded along slit, will combine all files | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
else
    set nscombine_files = ${startdir}/LISTS/targetA.lst
    set sky_files = ${startdir}/LISTS/targetB.lst
    echo Science target observations nodded off slit, will combine only A beam files | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
endif

#Next, combine the (on-source) science target files into a single 2D spectrum; same for standard star
pyraf_python ${procdir}/CombineSpectraXD.py ${startdir}/LISTS/standard.lst ${nscombine_files} ${error_spec} ${sky_files} | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt

set temp = `grep 'Processing' ${startdir}/PRODUCTS/XDGNIRS_Log.txt | tail -n 1`
sed -n -e "/$temp/"',$p' ${startdir}/PRODUCTS/XDGNIRS_Log.txt > error_record.txt
set error_check = `grep 'Error\|ERROR' error_record.txt | grep -v '502\|NSFLAT ERROR'`
if("$error_check" != "") then
        goto clean_exit
endif
if (${exit_point} == 6) then
    goto user_exit
endif
cd ${startdir}

loop7: 

echo "*************************************" | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
echo "**   Extract Spectra               **" | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
echo "*************************************" | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt


(rm -f ${startdir}/INTERMEDIATE/nsextract.cl )  >& /dev/null
ln -s ${procdir}/nsextract.cl ${startdir}/INTERMEDIATE/.
(rm -f ${startdir}/INTERMEDIATE/database/ap*) >& /dev/null 
(rm -f ${startdir}/INTERMEDIATE/v*comb.fits) >& /dev/null 
(rm -f ${startdir}/INTERMEDIATE/a*comb.fits) >& /dev/null 
(rm -f ${startdir}/INTERMEDIATE/s?target_comb.fits) >& /dev/null 
(rm -f ${startdir}/INTERMEDIATE/trace_ref.fits) >& /dev/null

#Find out whether the observations nodded to sky.
#If not, shouldn't extract full-slit spectra and spectra stepped along the slit; warn about that.
set nod_check = `grep 'Observations nodded off slit' ${startdir}/LISTS/nod_to_sky.txt | awk '{print $1}'`
if (${nod_check} == "") then
    set nod_to_sky = "no"
else
    set nod_to_sky = "yes"
endif
if (${nod_to_sky} == "no" && ${extras} == "yes") then
    echo "***WARNING: You are trying to extract full-slit and stepwise spectra from data with A and B beams both on the slit" | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
    echo "This is unlikely to give good results, and may crash the script" | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
    echo "It is recommended that you restart from the extraction step with 'extras=no'" | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
endif

cd ${startdir}/INTERMEDIATE/

#Use ExtractSpectraXD.py to extract spectra as specified by the input options
pyraf_python ${procdir}/ExtractSpectraXD.py target_comb.fits standard_comb.fits ${nsext_inter} '-'${aperture} ${aperture} ${extras} ${stepsize} ${gemiraf} ${procdir} sky_comb.fits ${error_spec} ${use_apall} | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
set temp = `grep 'Processing' ${startdir}/PRODUCTS/XDGNIRS_Log.txt | tail -n 1`
sed -n -e "/$temp/"',$p' ${startdir}/PRODUCTS/XDGNIRS_Log.txt > error_record.txt

#If nsextract exited with an "Aperture too large" error, ignore it because we should have been able to work round it and exit successfully anyway
set error_check = `grep 'Error\|ERROR' error_record.txt | grep -v '502\|NSFLAT ERROR\|Aperture too large'`
echo 'HERE'
echo $error_check
if("$error_check" != "") then
        goto clean_exit
endif
if (${exit_point} == 7) then
    goto user_exit
endif
cd ${startdir}


loop8: 

echo "*************************************" | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
echo "**      H Line Removal             **" | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
echo "*************************************" | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt

(rm -f ${startdir}/INTERMEDIATE/vega_ext.fits) >& /dev/null 
(cp  ${procdir}/vega_ext.fits ${startdir}/INTERMEDIATE/.) >& /dev/null 
(rm -f ${startdir}/INTERMEDIATE/reg_*.dat) >& /dev/null 
(ln -s  ${procdir}/reg_tell1.dat ${startdir}/INTERMEDIATE/.) >& /dev/null 
(ln -s  ${procdir}/reg_cont.dat ${startdir}/INTERMEDIATE/.) >& /dev/null 
(ln -s  ${procdir}/cur*.txt ${startdir}/INTERMEDIATE/.) >& /dev/null
(rm -f ${startdir}/INTERMEDIATE/ftell_nolines*) >& /dev/null
cd ${startdir}/INTERMEDIATE/

#Remove the H absorption lines from the standard star so they don't create spurious "emission lines" in the science target data
#In principle the linefit_manual option could be used to remove other features from the star
#If hlines=none, HlinesXD.py is still called just to create a file with the right name for the next step
pyraf_python ${procdir}/HlinesXD.py standard_comb.fits  ${telluric1_inter} ${hlines} | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt

set temp = `grep 'Processing' ${startdir}/PRODUCTS/XDGNIRS_Log.txt | tail -n 1`
sed -n -e "/$temp/"',$p' ${startdir}/PRODUCTS/XDGNIRS_Log.txt > error_record.txt
set error_check = `grep 'Error\|ERROR' error_record.txt | grep -v '502\|NSFLAT ERROR\|Aperture too large'`
if("$error_check" != "") then
        goto clean_exit
endif
if (${exit_point} == 8) then
    goto user_exit
endif
cd ${startdir}


loop9: 

echo "*************************************" | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
echo "**      Telluric Cancellation      **" | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
echo "*************************************" | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt

#if spectra were extracted in steps along the slit (extras=yes), want to know how many (so can remove telluric lines from all of them)
if (-e ${startdir}/INTERMEDIATE/nsteps.txt) then
    set nsteps = `cat ${startdir}/INTERMEDIATE/nsteps.txt`
else
    set nsteps = "N/A"
endif

#Check for inconsistencies in the relevant input options
#This should really go into CheckOptions.py - WISHLIST
if ($extras == 'yes' && $nsteps == 'N/A') then
    echo ''
    echo 'XDGNIRS ERROR: You are trying to calibrate spectra extracted in steps along the slit, but you only extracted the central spectrum in step 7. Please either redo step 7 with extras=yes, or restart this step with extras=no (the default).'
    echo ''
    goto clean_exit
endif

(rm -f ${startdir}/INTERMEDIATE/fit*) >& /dev/null 
(rm -f ${startdir}/INTERMEDIATE/*norm*) >& /dev/null 
(rm -f ${startdir}/INTERMEDIATE/atell*) >& /dev/null
(rm -f ${startdir}/INTERMEDIATE/vtell*) >& /dev/null
(rm -f ${startdir}/INTERMEDIATE/s?tell*) >& /dev/null
(rm -f ${startdir}/INTERMEDIATE/f?tell*) >& /dev/null
(rm -f ${startdir}/INTERMEDIATE/fs?tell*) >& /dev/null
(rm -f ${startdir}/INTERMEDIATE/reg_*.dat) >& /dev/null 
(ln -s  ${procdir}/reg_tell2.dat ${startdir}/INTERMEDIATE/.) >& /dev/null 
(ln -s  ${procdir}/reg_cont.dat ${startdir}/INTERMEDIATE/.) >& /dev/null 
(ln -s  ${procdir}/cur*.txt ${startdir}/INTERMEDIATE/.) >& /dev/null
cd ${startdir}/INTERMEDIATE/

#Use TelluricXD.py to run IRAF's "telluric" task on each extension
pyraf_python ${procdir}/TelluricXD.py target_comb.fits standard_comb.fits ${telluric2_inter} ${continuum_inter} ${extras} ${nsteps} | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt

set temp = `grep 'Processing' ${startdir}/PRODUCTS/XDGNIRS_Log.txt | tail -n 1`
sed -n -e "/$temp/"',$p' ${startdir}/PRODUCTS/XDGNIRS_Log.txt > error_record.txt
set error_check = `grep 'Error\|ERROR' error_record.txt | grep -v '502\|NSFLAT ERROR\|Aperture too large'`
if("$error_check" != "") then
        goto clean_exit
endif

if (${exit_point} == 9) then
    goto user_exit
endif
cd ${startdir}


loop10: 

echo "*************************************" | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
echo "**      Flux Calibration           **" | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
echo "*************************************" | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt


#Use mag2mass.py to look up the standard star magnitudes and spectral type if that information isn't already in a file
if (! -e ${startdir}/INTERMEDIATE/std_star.txt || -z ${startdir}/INTERMEDIATE/std_star.txt)  then
    echo "##### identifying the standard star and getting magnitudes ...."
    set ra_std = `cat ${startdir}/LISTS/ID${infile}  | grep partnerCal | grep OBJECT | head -1 | awk '{print $11}'`
    set dec_std = `cat ${startdir}/LISTS/ID${infile}  | grep partnerCal | grep OBJECT | head -1 | awk '{print $12}'`
    set sign = `echo ${dec_std} | cut -c1` 
    if ("${sign}" != "+" && "${sign}" != "-") then
    set dec_std = "+"${dec_std}
    endif
    pyraf_python ${procdir}/mag2mass.py ${ra_std}${dec_std} ${startdir}/INTERMEDIATE/std_star.txt ${procdir}/starstemp.txt | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
endif

set temp = `grep 'Processing' ${startdir}/PRODUCTS/XDGNIRS_Log.txt | tail -n 1`
sed -n -e "/$temp/"',$p' ${startdir}/PRODUCTS/XDGNIRS_Log.txt > error_record.txt
set error_check = `grep 'Error\|ERROR' error_record.txt | grep -v '502\|NSFLAT ERROR\|Aperture too large'`
if("$error_check" != "") then
    #if there were errors, probably want to remove the dodgy standard star file so it's not sitting there causing problems the next time round
    (rm -f ${startdir}/INTERMEDIATE/std_star.txt) >& /dev/null
    goto clean_exit
endif

#if spectra were extracted in steps along the slit (extras=yes), want to know how many (so can calibrate them all)
if (-e ${startdir}/INTERMEDIATE/nsteps.txt) then
    set nsteps = `cat ${startdir}/INTERMEDIATE/nsteps.txt`
else
    set nsteps = "N/A"
endif

#Check for inconsistencies in the relevant input options
#This should really go into CheckOptions.py - WISHLIST
if ($extras == 'yes' && $nsteps == 'N/A') then
    echo ''
    echo 'XDGNIRS ERROR: You are trying to calibrate spectra extracted in steps along the slit, but you only extracted the central spectrum in step 7. Please either redo steps 7 and 8 with extras=yes, or restart this step with extras=no (the default).'
    echo ''
    goto clean_exit
endif

(rm -f ${startdir}/INTERMEDIATE/*bb*fits) >& /dev/null 
(rm -f ${startdir}/INTERMEDIATE/flam*fits) >& /dev/null 
#(rm -f ${startdir}/INTERMEDIATE/*tell*) >& /dev/null 
(rm -f ${startdir}/INTERMEDIATE/flam*fits) >& /dev/null 
(rm -f ${startdir}/INTERMEDIATE/${target_name}*) >& /dev/null 
(rm -f ${startdir}/INTERMEDIATE/${standard_name}*) >& /dev/null 
(rm -f ${startdir}/INTERMEDIATE/vega_ext.fits) >& /dev/null 
(cp  ${procdir}/vega_ext.fits ${startdir}/INTERMEDIATE/.) >& /dev/null 

cd ${startdir}/INTERMEDIATE/

#Use FluxCalib.py to do absolute flux calibration if the information is available, otherwise just relative flux cal
#Should really also have absolute vs relative flux cal as input option - WISHLIST
pyraf_python ${procdir}/FluxCalibXD.py target_comb.fits  standard_comb.fits ${extras} ${nsteps} | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt

set temp = `grep 'Processing' ${startdir}/PRODUCTS/XDGNIRS_Log.txt | tail -n 1`
sed -n -e "/$temp/"',$p' ${startdir}/PRODUCTS/XDGNIRS_Log.txt > error_record.txt
set error_check = `grep 'Error\|ERROR' error_record.txt | grep -v '502\|NSFLAT ERROR\|Aperture too large'`
if("$error_check" != "") then
        goto clean_exit
endif

if (${exit_point} == 10) then
    goto user_exit
endif
cd ${startdir}


loop11: 

echo "*************************************" | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
echo "**      Combining Orders           **" | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
echo "*************************************" | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt


set target_name = `cat ${startdir}/LISTS/sci_name.txt`
set standard_name = `cat ${startdir}/LISTS/std_name.txt`

#If the user wants to shift the spectra to rest, and the redshift isn't already recorded in a file, use redshift.py to look it up
#Otherwise, just set it to zero
if (${deredshift} == "yes") then
    if (! -e ${startdir}/INTERMEDIATE/redshift.txt) then
        echo "##### retrieving the science target's redshift"
        set ra_galaxy = `cat ${startdir}/LISTS/ID${infile}  | grep science | grep OBJECT | head -1 | awk '{print $11}'`
        set dec_galaxy = `cat ${startdir}/LISTS/ID${infile}  | grep science | grep OBJECT | head -1 | awk '{print $12}'`
        set sign = `echo $dec_galaxy | cut -c1` 
        if ("$sign" != "+" && "$sign" != "-") then
            set dec_galaxy = "+"${dec_galaxy}
        endif
        pyraf_python ${procdir}/redshift.py ${ra_galaxy}${dec_galaxy} ${startdir}/INTERMEDIATE/redshift.txt
    endif
    set redshift = `cat ${startdir}/INTERMEDIATE/redshift.txt`
    echo $redshift | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
else
    set redshift = 0
endif

set temp = `grep 'Processing' ${startdir}/PRODUCTS/XDGNIRS_Log.txt | tail -n 1`
sed -n -e "/$temp/"',$p' ${startdir}/PRODUCTS/XDGNIRS_Log.txt > error_record.txt
set error_check = `grep 'Error\|ERROR' error_record.txt | grep -v '502\|NSFLAT ERROR\|Aperture too large'`
if("$error_check" != "") then
    #if there were errors, probably want to remove the dodgy redshift file so it's not sitting there causing problems the next time round
    (rm -f ${startdir}/INTERMEDIATE/redshift.txt) >& /dev/null
        goto clean_exit
endif

#if spectra were extracted in steps along the slit (extras=yes), want to know how many (so can shift them all to rest and/or combine them all)
if (-e ${startdir}/INTERMEDIATE/nsteps.txt) then
    set nsteps = `cat ${startdir}/INTERMEDIATE/nsteps.txt`
else
    set nsteps = "N/A"
endif

#Check for inconsistencies in the relevant input options
#This should really go into CheckOptions.py - WISHLIST
if ($extras == 'yes' && $nsteps == 'N/A') then
    echo ''
    echo 'XDGNIRS ERROR: You are trying to stitch together spectra extracted in steps along the slit, but you only extracted the central spectrum in step 7. Please either redo steps 7, 8 and 9 with extras=yes, or restart this step with extras=no (the default).'
    echo ''
    goto clean_exit
endif

if (! -e ${startdir}/INTERMEDIATE/goodbits.txt) then
    (cp  ${procdir}/goodbits.txt ${startdir}/INTERMEDIATE/) >& /dev/null
endif

(rm -f ${startdir}/PRODUCTS/${target_name}*) >& /dev/null 
(rm -f ${startdir}/PRODUCTS/${standard_name}*) >& /dev/null
(rm -f ${startdir}/PRODUCTS/orders*) >& /dev/null
(rm -f ${startdir}/INTERMEDIATE/odcombine_*) >& /dev/null 
(rm -f ${startdir}/INTERMEDIATE/offsets.log) >& /dev/null 
(rm -f ${startdir}/INTERMEDIATE/final*fits) >& /dev/null  
(rm -f ${startdir}/INTERMEDIATE/sky.fits) >& /dev/null
(rm -f ${startdir}/INTERMEDIATE/sky_rest.fits) >& /dev/null
(rm -f ${startdir}/INTERMEDIATE/sky*txt) >& /dev/null
cd ${startdir}/INTERMEDIATE/

#Use CombineOrdersXD.py to combine the spectral orders into a single spectrum
#pyraf_python ${procdir}/CombineOrdersXD.py ${redshift} ${deredshift} ${extras} ${nsteps} ${offsets} ${error_spec} ${version} | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
pyraf_python ${procdir}/CombineOrdersXD.py ${redshift} ${deredshift} ${extras} ${nsteps} ${offsets} ${error_spec} ${version} ${wave_soln_linear}| & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt

set temp = `grep 'Processing' ${startdir}/PRODUCTS/XDGNIRS_Log.txt | tail -n 1`
sed -n -e "/$temp/"',$p' ${startdir}/PRODUCTS/XDGNIRS_Log.txt > error_record.txt
set error_check = `grep 'Error\|ERROR' error_record.txt | grep -v '502\|NSFLAT ERROR\|Aperture too large'`
if("$error_check" != "") then
        goto clean_exit
endif
if (${exit_point} == 11) then
    goto user_exit
endif
cd ${startdir}


loop12: 

echo "*************************************" | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
echo "**    Calculating Noise Spectrum   **" | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
echo "*************************************" | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt

#This hasn't been thoroughly tested and I don't yet trust the results
#Will only work on observations nodded to blank sky 

if ($error_spec == 'yes') then
    (rm -f ${startdir}/PRODUCTS/*_spec.txt) >& /dev/null
    (rm -f ${startdir}/PRODUCTS/*_noise.txt) >& /dev/null
    (rm -f ${startdir}/PRODUCTS/*_snr.txt) >& /dev/null
    (rm -f ${startdir}/INTERMEDIATE/snoise.fits) >& /dev/null
    (rm -f ${startdir}/INTERMEDIATE/target_noise.fits) >& /dev/null

    
    set target_name = `cat ${startdir}/LISTS/sci_name.txt`
    if (-e ${startdir}/PRODUCTS/${target_name}_rest.fits) then
	set refspec = ${target_name}_rest.fits
    else if (! -e ${startdir}/PRODUCTS/${target_name}_rest.fits && -e ${startdir}/PRODUCTS/${target_name}.fits) then
	set refspec = ${target_name}.fits
    else
	echo ""
	echo "ERROR: Can't find the reduced science target spectrum, which is needed for creating the error spectrum. " | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
	echo ""
    endif
    set temp = `grep 'Processing' ${startdir}/PRODUCTS/XDGNIRS_Log.txt | tail -n 1`
    sed -n -e "/$temp/"',$p' ${startdir}/PRODUCTS/XDGNIRS_Log.txt > error_record.txt
    set error_check = `grep 'Error\|ERROR' error_record.txt | grep -v '502\|NSFLAT ERROR\|Aperture too large'`
    if("$error_check" != "") then
	goto clean_exit
    endif

    set nod_check = `grep 'Observations nodded off slit' ${startdir}/LISTS/nod_to_sky.txt | awk '{print $1}'`
    if (${nod_check} == "") then
	set nod_to_sky = "no"
    else
	set nod_to_sky = "yes"
    endif
    if ${nod_to_sky} == "yes" then
	cd ${startdir}/INTERMEDIATE/ 
	pyraf_python ${procdir}/CalcNoiseXD.py vtarget_comb.fits ../PRODUCTS/${refspec} vsky_comb.fits $procdir | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
	set temp = `grep 'Processing' ${startdir}/PRODUCTS/XDGNIRS_Log.txt | tail -n 1`
	sed -n -e "/$temp/"',$p' ${startdir}/PRODUCTS/XDGNIRS_Log.txt > error_record.txt
	set error_check = `grep 'Error\|ERROR' error_record.txt | grep -v '502\|NSFLAT ERROR'`
	if("$error_check" != "") then
	    goto clean_exit
	endif
    else 
	#I added this because I was going to implement another method of estimating errors but didn't have time in the end
        echo "***WARNING: Observations not nodded to blank sky; skipping error calculation that includes sky counts"
    endif	    

    if (${exit_point} == 12) then
        goto user_exit
    endif
    cd ${startdir}
endif


loop13: 

echo "*************************************" | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
echo "**      Writing Data Sheet         **" | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
echo "*************************************" | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt

#Use GetInfoXD.py and WriteDataSheetXD.py to make a pdf with the spectrum and some information about counts, S/N, conditions etc.
#This is the "data sheet" pdf in the PRODUCTS directory

set target_name = `cat ${startdir}/LISTS/sci_name.txt`
if (! -r ${startdir}/INTERMEDIATE/redshift.txt) then
    set redshift = 0
    echo "No redshift info found, science target and Vega model spectra may be offset in wavelength in final plot" | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
    echo "If this is a problem, rerun starting from spectral extraction step with shift_to_rest=yes" | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
else if (-r ${startdir}/INTERMEDIATE/redshift.txt) then
    set redshift = `cat ${startdir}/INTERMEDIATE/redshift.txt`
endif

set relative_flux = `grep -m 1 'N/A' ${startdir}/INTERMEDIATE/std_star.txt | awk '{print $3}'`
if (${relative_flux} == "") then
    set absunits = "yes"
else
    set absunits = "no"
endif

set nod_check = `grep 'Observations nodded off slit' ${startdir}/LISTS/nod_to_sky.txt | awk '{print $1}'`
if (${nod_check} == "") then
    set nod_to_sky = "no"
else
    set nod_to_sky = "yes"
endif

cd ${startdir}/INTERMEDIATE/
(rm -f target.log) >& /dev/null
(rm -f standard.log) >& /dev/null
(rm -f target_info.txt) >& /dev/null
(rm -f standard_info.txt) >& /dev/null
(rm -f galdiv1.fits) >& /dev/null
(rm -f stardiv1.fits) >& /dev/null
(rm -f snr*.txt) >& /dev/null
(rm -f  bplotcur.txt) >& /dev/null
(rm -f  ../PRODUCTS/*_data_sheet.pdf) >& /dev/null
echo '21000 0 1 m' > bplotcur.txt
echo '22000 0 1 m' >> bplotcur.txt
echo 'q' >> bplotcur.txt

pyraf_python ${procdir}/GetInfoXD.py ${startdir}/LISTS/targetA.lst ${startdir}/LISTS/targetB.lst ${startdir}/LISTS/standardA.lst ${startdir}/LISTS/standardB.lst ${nod_to_sky} | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
pyraf_python ${procdir}/WriteDataSheetXD.py ${procdir}/vega_all.txt ${startdir}/PRODUCTS/${target_name} ${version} ${date} ${redshift} ${absunits} ${startdir} | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt

set temp = `grep 'Processing' ${startdir}/PRODUCTS/XDGNIRS_Log.txt | tail -n 1`
sed -n -e "/$temp/"',$p' ${startdir}/PRODUCTS/XDGNIRS_Log.txt > error_record.txt
set error_check = `grep 'Error\|ERROR' error_record.txt | grep -v '502\|NSFLAT ERROR\|Aperture too large'`
if("$error_check" != "") then
        goto clean_exit
endif
if (${exit_point} == 13) then
    goto loop_end
endif
cd ${startdir}


############
#TIDYING UP
############

loop_end: 

(rm -f ${startdir}/INTERMEDIATE/tmp*) >& /dev/null 

echo "**     Ending ....   **" | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
set timenow = `date +%F" "%T`
echo " Processing list of files "${infile}": Finished at "${timenow} | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
echo ""  | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
echo "" >> ${startdir}/PRODUCTS/Checks_and_Warnings.txt
grep -F \*WARNING ${startdir}/PRODUCTS/XDGNIRS_Log.txt >> ${startdir}/PRODUCTS/Checks_and_Warnings.txt
echo "" >> ${startdir}/PRODUCTS/Checks_and_Warnings.txt
grep -F \*CHECK ${startdir}/PRODUCTS/XDGNIRS_Log.txt >> ${startdir}/PRODUCTS/Checks_and_Warnings.txt
echo "" >> ${startdir}/PRODUCTS/Checks_and_Warnings.txt
(rm -f ${startdir}/INTERMEDIATE/tmp*) >& /dev/null 

exit

#Exit with a hopefully helpful error message if problems were found
clean_exit:
    cd ${startdir}
    echo ""  | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
    echo ">>>>>>>>>>>XDGNIRS has encountered a problem:"  | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
    echo ">>>>>>>>>>>$error_check"  | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
    echo ">>>>>>>>>>>Check LISTS/Status_$infilen.txt"  | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
    echo ">>>>>>>>>>>Stopping the script."  | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
    echo ""  | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
    echo "" >> ${startdir}/PRODUCTS/Checks_and_Warnings.txt
    grep -F \*WARNING ${startdir}/PRODUCTS/XDGNIRS_Log.txt >> ${startdir}/PRODUCTS/Checks_and_Warnings.txt
    echo "" >> ${startdir}/PRODUCTS/Checks_and_Warnings.txt
    grep -F \*CHECK ${startdir}/PRODUCTS/XDGNIRS_Log.txt >> ${startdir}/PRODUCTS/Checks_and_Warnings.txt
    echo "" >> ${startdir}/PRODUCTS/Checks_and_Warnings.txt
    (rm -f ${startdir}/INTERMEDIATE/tmp*) >& /dev/null 
    rm error_record.txt >& /dev/null
    exit

#Or just exit because the user said so
user_exit:
    cd ${startdir}
    echo ""  | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
    echo ">>>>>>>>>>>Stopping the script after step ${exit_point} at user's request"  | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
    set timenow = `date +%F" "%T`
    echo " Processing list of files "${infile}": Finished at "${timenow} | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt ${startdir}/PRODUCTS/Checks_and_Warnings.txt
    echo ""  | & tee -a ${startdir}/PRODUCTS/XDGNIRS_Log.txt
    echo "" >> ${startdir}/PRODUCTS/Checks_and_Warnings.txt
    grep -F \*WARNING ${startdir}/PRODUCTS/XDGNIRS_Log.txt >> ${startdir}/PRODUCTS/Checks_and_Warnings.txt
    echo "" >> ${startdir}/PRODUCTS/Checks_and_Warnings.txt
    grep -F \*CHECK ${startdir}/PRODUCTS/XDGNIRS_Log.txt >> ${startdir}/PRODUCTS/Checks_and_Warnings.txt
    echo "" >> ${startdir}/PRODUCTS/Checks_and_Warnings.txt
    (rm -f ${startdir}/INTERMEDIATE/tmp*) >& /dev/null 
    rm error_record.txt >& /dev/null
    exit
