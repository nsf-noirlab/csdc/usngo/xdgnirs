#!/usr/bin/python
#
#Look up target redshift
#
# Import libraries
#

import sys
from astroquery.simbad import Simbad
import astropy.coordinates as coord
import astropy.units as u
from astropy.table import Table
import numpy as np


#Construct URL based on science target coords, execute SIMBAD query to find spectral type
name = str(sys.argv[1:2][0])
Simbad.add_votable_fields('flux(K)', 'sp', 'rvz_radvel', 'rvz_type')
star_table = Simbad.query_region(name, radius=0.01 * u.deg)
print( star_table)
Rvel = str(star_table['RVZ_RADVEL'][0])
Type = star_table['RVZ_TYPE'][0]

print()
print(Rvel)
print()

if Type == 'z':
    Rvel = Rvel

elif Type == 'v':
    c = 299792 #km/s
    numerator   = 1 + (float(Rvel)/c)
    denominator = 1 - (float(Rvel)/c)
    Rvel = round(np.sqrt(numerator/denominator)-1,6)
    print('Radial Velocity Redshift = %f'%Rvel)
    print(Type)

else:
    print(Type)
    Rvel = 'Warning:_no_redshift_found,_not_shifting_to_rest_frame'
    print(Rvel)

redshiftfile = sys.argv[2:3][0]
zfile = open(redshiftfile,'w')  
zfile.write (Rvel+'\n')  
zfile.close()


