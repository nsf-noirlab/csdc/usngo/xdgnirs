#!/usr/bin/python
#
# Look up magnitude, spectral type, and temperature of standard star, given its coordinates
#
# Import libraries
#
#import sgmllib
#import sgmllib3k
import sys
#import urllib2
import re
from astroquery.simbad import Simbad
import astropy.coordinates as coord
from astropy.table import Table

starfile = sys.argv[2:3][0]
kelvinfile = sys.argv[3:4][0]

sf = open(starfile,'w')
klf = open(kelvinfile)

#Construct URL based on standard star coords, execute SIMBAD query to find spectral type
name = str(sys.argv[1:2][0])
Simbad.add_votable_fields('flux(K)', 'sp')
star_table = Simbad.query_region(name)
Kmag = str(star_table['FLUX_K'][0])
#Get spectral type, take only first 3 characters (i.e., strip off end of types like AIVn as they're not in the starstemp table)
spectral_type = star_table['SP_TYPE'][0][0:3]
#Find temperature for this spectral type 
count = 0
for line in klf:
	if '#' in line:
            continue
	else:
            if spectral_type in line.split()[0]:
                kelvin = line.split()[1] 
                count = 0
                break
            else:
                count+=1
if count > 0:
    print( "ERROR: can't find a temperature for spectral type", spectral_type,". You'll need to supply information in a file; see the manual for instructions.")	
    sys.exit()

sf.write('k K '+Kmag+' '+kelvin+'\n')
#not sure if the rest of these lines are necessary now we're not using "sensfunc" etc. for flux calibration
sf.write('h H '+Kmag+' '+kelvin+'\n')
sf.write('j J '+Kmag+' '+kelvin+'\n')
sf.write('j J '+Kmag+' '+kelvin+'\n')
sf.write('i J '+Kmag+' '+kelvin+'\n')
sf.write('i J '+Kmag+' '+kelvin+'\n')

sf.close()
klf.close()

