#!/usr/bin/env python
#
##
##
##	HlinesXD.py: removing the H lines from the standard star 		
##
##
#
# Import libraries
#


import sys
from pyraf import *
import astropy.io.fits
import numpy
from astropy.io.fits import getdata, getheader
import matplotlib.pyplot as plt
#iraf.reset(gemini="extern$gemini_public_1_11_1/")
iraf.gemini(_doprint=0, motd="no")
iraf.gnirs(_doprint=0)
iraf.imutil(_doprint=0)
iraf.onedspec(_doprint=0)
iraf.nsheaders('gnirs',Stdout='/dev/null')

#Define the combined standard star data file
standards = sys.argv[1:2][0]

#Whether or not to do things interactively
telluricinter = sys.argv[2:3][0]

#How to remove the H lines from the standard star
hline_method = sys.argv[3:4][0]

obs=astropy.io.fits.open('v'+standards)
airmass_standard = obs[0].header["AIRMASS"]

#Regions used in "telluric" task for removing H lines from standard star
file = open('reg_tell1.dat','r')    
tell1 = file.readlines()

#Files for recording shift/scale from calls to "telluric"
t1 = open('../LISTS/telluric_hlines.txt', 'w')

def write_line_positions (nextcur,var):
    curfile = open (nextcur, "w")
    i = -1
    for line in var:
        i += 1
        if i != 0:
            var[i] = var[i].split()
            var[i][2] = var[i][2].replace("',","").replace("']","")
        if not i%2 and i != 0:
                        #even number, means RHS of H line
                        #write x and y position to file, also "k" key
            curfile.write (var[i][0]+" "+var[i][2]+" 1 k \n")
                        #LHS of line, write info + "l" key to file
            curfile.write (var[i-1][0]+" "+var[i-1][2]+" 1 l \n")
                        #now repeat but writing the "-" key to subtract the fit
            curfile.write (var[i][0]+" "+var[i][2]+" 1 - \n")
            curfile.write (var[i-1][0]+" "+var[i-1][2]+" 1 - \n")
    curfile.write ("0 0 1 i \n")
    curfile.write ("0 0 1 q \n")
    curfile.close()


#Options for removing H lines from standard star

def vega(spectrum, ext):
    #Use "telluric" to remove H lines from standard star, then remove normalisation added by telluric
    #Will be done interactively if telluric1_inter=yes
    tell_info = iraf.telluric(input='v'+spectrum+'[sci,'+str(ext)+']',output='tell_nolines'+str(ext),cal='vega_ext.fits['+str(ext)+']',airmass=airmass_standard,answer='yes',ignoreaps='yes',xcorr='yes',tweakrms='yes',inter=telluricinter,sample=tell_sample1,threshold=0.1,lag=3,shift=0.,dshift=0.0,scale=1.,dscale=0.0,offset=0,smooth=1,cursor='',mode='al',Stdout=1)
    #record shift & scale info for future reference
    t1.write(str(tell_info) + '\n')
    #need this loop to identify telluric output containing warning about pix outside calibration limits (different formatting)
    if "limits" in tell_info[-1].split()[-1]:
        norm = tell_info[-2].split()[-1]
    else:
        norm = tell_info[-1].split()[-1]
    iraf.imarith (operand1='tell_nolines'+str(ext),op="/",operand2=norm,result='ftell_nolines'+str(ext),title='',divzero=0.0,hparams='',pixtype='',calctype='',verbose='no',noact='no',mode='al')


def linefit_auto(spectrum, ext):
    #automatically fit Lorentz profiles to lines defined in existing cur* files
    #Go to x pos in cursor file and use space bar to find spectrum at each of those points
    specpos = iraf.bplot(images='v'+spectrum+'[sci,'+str(ext)+']',cursor='cur'+str(ext)+'.txt',Stdout=1,StdoutG='/dev/null')
    specpos = str(specpos).split("'x,y,z(x):")
    nextcur = 'nextcur'+str(ext)+'.txt'
    #Write line x,y info to file containing Lorentz fitting commands for bplot
    write_line_positions (nextcur, specpos)
    iraf.delete('ftell_nolines'+str(ext)+'.fits,Lorentz'+str(ext),ver="no",go_ahead='yes',Stderr='/dev/null')
    #Fit and subtract Lorentz profiles. Might as well write output to file.
    iraf.bplot(images='vstandard_comb[sci,'+str(ext)+']',cursor='nextcur'+str(ext)+'.txt', new_image='ftell_nolines'+str(ext), overwrite="yes",StdoutG='/dev/null',Stdout='Lorentz'+str(ext))


def linefit_manual(spectrum, ext):
    #Enter splot so the user can fit and subtract lorentz (or, actually, any) profiles 
    iraf.splot (images=spectrum, new_image="ftell_nolines"+str(ext), save_file="../LISTS/lorentz_hlines.txt", overwrite="yes")
    #it's easy to forget to use the "i" key to actually write out the line-free spectrum, so check that it exists:
    #with the "tweak" options, the line-free spectrum will already exist, so this lets the user simply "q" and move on w/o editing (too bad if they edit and forget to hit "i"...)
    while True: 
        try:
            with open("ftell_nolines"+str(ext)+".fits") as f: pass
            break
        except IOError as e:
            print( "It looks as if you didn't use the i key to write out the lineless spectrum. We'll have to try again. --> Re-entering splot")
            iraf.splot (images=spectrum, new_image="ftell_nolines"+str(ext), save_file="../LISTS/lorentz_hlines.txt", overwrite="yes")


#For each order, remove H lines from standard star 
for j in range(1,7):
    tell_sample1 = '"'+tell1[j-1].replace('\n','')+'"'
    iraf.hedit(images='v'+standards+'[sci,'+str(j)+']',fields='AIRMASS',value=airmass_standard,add='yes',addonly='no',delete='no',verify='no',show='no',update = 'yes',mode='al') 

    if hline_method == "none":
        #need to copy files so have right names for later use
        iraf.imcopy(input='v'+standards+'[sci,'+str(j)+']', output="ftell_nolines"+str(j), verbose='no')

    if hline_method != "none":
        print( "")
        print( "***Removing intrinsic lines in standard star, extension", str(j) )
        print( "")
    
    if hline_method == "vega":
        vega(standards,j)

    if hline_method == "linefit_auto":
        linefit_auto(standards,j)

    if hline_method == "linefit_manual":
        linefit_manual("v"+standards+'[sci,'+str(j)+']', j)

    if hline_method == "vega_tweak":
        #run vega removal automatically first, then give user chance to interact with each order as well
        vega(standards,j)
        linefit_manual("ftell_nolines"+str(j), j)

    if hline_method == "linefit_tweak":
        #run Lorentz removal automatically first, then give user chance to interact with each order as well
        linefit_auto(standards,j)
        linefit_manual("ftell_nolines"+str(j), j)


file.close()
t1.close()
