The GNIRSXD pipeline code was originally created by Rachel Mason.

The version of the code contained in this directory has been editted to be compliant with Python 3.7.11 and has been verified to be in working order as of September 2022. 

NOTE: This pipeline uses the latest version of cleanir.py (last updated on January 13, 2022)
