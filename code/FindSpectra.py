#Called by XDpiped.csh
#Rewritten by REM November 2015

import sys, os
from pyraf import *
import astropy.io.fits
from astropy.io.fits import getdata, getheader

acqtarget = sys.argv[1:2][0]
acqstd = sys.argv[2:3][0]
target = sys.argv[3:4][0]
standard = sys.argv[4:5][0]
tgtA = sys.argv[5:6][0]
tgtB = sys.argv[6:7][0]
temp = sys.argv[7:8][0]
qfile = sys.argv[8:9][0]


#    1) determine the size of the nod so XDiped.csh can find out whether the observation nodded off to blank sky
#       that info is needed for e.g. knowing whether to shift and combine all files or just "A" beam ones
#    2) determine the absolute location of the spectra along the slit
#       so if the object is too faint for automatic extraction, can extract columns around that point instead
#    3) we might as well check some exposure times/coadds while we're here, too

#The Q offsets in the OT aren't recorded in the headers, need to subtract the offsets at the end of the acquisitions from those in the spectral files to recover those values :(

def get_acq_offsets(acqfile):
    file = open(acqfile,'r')
    j = 0
    for line in file.readlines():
        line = line.replace('\n','')
        obs = astropy.io.fits.open(line)
        acq_offset = obs[0].header["QOFFSET"]
        #acq_offset is where the telescope was (in Q) at the end of the acquisition
        j += 1
    if j == 0:
        print( 'ERROR: no acquisition images listed in', acqfile)
        sys.exit(0)
    return acq_offset

def get_AB_offsets(specfiles, aoff, obj):
    file = open(specfiles,'r')
    lines = file.readlines()
    offset = []
    count = -1
    exp = []
    coadds = []
    ExpOK = "True"
    CoaddsOK = "True"
    for line in lines:
        #find the Q offsets in the headers
        obs = astropy.io.fits.open(line.replace('\n',''))
        qoff = obs[0].header["QOFFSET"]
        #transform them into the Q offsets in the OT (by subracting the offset at the end of the acquisition)
        ot_qoff = qoff - aoff	
        offset.append(ot_qoff)	

        #also, let's check that the exposure times and coadds didn't change partway through the sequence
        #might happen if the observer saw saturation in the standard and lowered the exposure time, for example
        count += 1
        exp.append (obs[0].header["EXPTIME"])
        coadds.append (obs[0].header["COADDS"])
        if count != 0 and exp[count] != exp[count-1]:
            ExpOK = "False"
            break
        if count != 0 and coadds[count] != coadds[count-1]:
            CoaddsOK = "False"
            break 

    if ExpOK == "True" and CoaddsOK == "True":
        print( "Texp/coadds OK for "+obj)
    else: 
        print( "ERROR: Texp/coadds change partway through the "+obj+" observation. Remake your lists using only files with the desired Texp/Coadds" )

    #find the nod size in arcseconds, and determine whether it was a nod to blank sky (off the slit)
    #for historical reasons, XDpiped.csh expects this info to be in a file called nod_to_sky.txt
    #assume that standard stars are always nodded along slit; only write this for science target
    nod = max(offset) - min(offset)
    if obj == 'Science target':
        nodfile = open ('LISTS/nod_to_sky.txt', 'w')
        #Don't change this wording or there will be trouble later!!
        #Or, if you change this make sure to also update CombineSpectraXD.py where it greps for stuff in nod_to_sky.txt
        if abs(nod) >= 3.5:
            nodfile.write("Observations nodded off slit; Q offset = "+str(nod)+" arcsec")
        else:
            nodfile.write("Observations nodded along slit; Q offset = "+str(nod)+" arcsec")
        nodfile.close()

        #One more thing (for science target only):
        #For obs nodded off slit, need to check that first file is actually in the A position (is on the slit)
        #If sequence is interrupted and restarted part way through an ABBA, data set can start on nod B
        #If this is wrongly assumed to be an "A" spectrum, nsextract will fail to find the peak and code will crash

        if offset[0] > 3.5:
            print( 'Sequence started in B beam, swapping targetA.lst and targetB.lst')
            os.rename(tgtA, temp)
            os.rename(tgtB, tgtA)
            os.rename(temp, tgtB)

    #Then, record the OT offsets so we can later figure out what column to expect the spectrum to be on
    #will be used by nsextract (ExtractSpectraXD.py) and WriteDataSheetXD.py
    #nscombine (SdistorAndWLcalibXD.py) shifts everything to the position of the first spectrum in the list given to it
    #so we only need the first value of QOFFSET

    qpos = str(offset[0])
    posfile = open (qfile, 'a')
    posfile.write(qpos+' \n')
    posfile.close()


tgt_aoff = get_acq_offsets(acqtarget) 
std_aoff = get_acq_offsets(acqstd)  

get_AB_offsets(target, tgt_aoff, 'Science target')
get_AB_offsets(standard, std_aoff, 'Standard star')



