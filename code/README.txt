++++++++++  To Install  ++++++++++

Include in your .bashrc: 

### GNIRS data reduction:
XDGNIRSDIR={PATH TO}/xdgnirs/
export XDGNIRSDIR
alias xdpiped="csh ${XDGNIRSDIR}/XDpiped.csh"

Include in your .cshrc: 

### GNIRS data reduction:
setenv XDGNIRSDIR   {PATH TO}/xdgnirs/
alias xdpiped "csh ${XDGNIRSDIR}/XDpiped.csh"


++++++++++  For Help  ++++++++++

xdpiped -h


++++++++++  Manual  ++++++++++

PATH/TO/xdgnirs/doc/XDGNIRS.pdf

