#!/usr/bin/env python
#
##
##
##	TelluricXD.py: remove telluric lines by dividing by standard star, using "telluric" task	
##
##
#
# Import libraries
#


import sys
from pyraf import *
import astropy.io.fits
import numpy
from astropy.io.fits import getdata, getheader
import matplotlib.pyplot as plt
#iraf.reset(gemini="extern$gemini_public_1_11_1/")
iraf.gemini(_doprint=0, motd="no")
iraf.gnirs(_doprint=0)
iraf.imutil(_doprint=0)
iraf.onedspec(_doprint=0)
iraf.nsheaders('gnirs',Stdout='/dev/null')

#Define the combined science target data file
target = sys.argv[1:2][0]
#Define the combined, lineless standard star data file
standard = sys.argv[2:3][0]

#Whether or not to do things interactively
telluricinter = sys.argv[3:4][0]
continuuminter = sys.argv[4:5][0]

#Full-slit and step-by-step extractions done? How many steps?
extras = sys.argv[5:6][0]
if extras == "yes":
    steps = int(sys.argv[6:7][0]) + 1

obs=astropy.io.fits.open('v'+standard)
airmass_standard = obs[0].header["AIRMASS"]
obs=astropy.io.fits.open('v'+target)
airmass_target = obs[0].header["AIRMASS"]

#Regions used in "telluric" task when dividing galaxy by standard
file = open('reg_tell2.dat','r')    
tell2 = file.readlines()
#Regions used in fitting continua to lineless standard star orders
file = open('reg_cont.dat','r')    
cont = file.readlines()
#Files for recording shift/scale from calls to "telluric"
t2 = open('../LISTS/telluric_telluric.txt', 'w')

for j in range(1,7):
    #fit continuum to lineless standard spectrum for use in call to telluric
    print( "")
    print( "***Fitting continuum to standard star, extension", str(j))
    print( "")
    #fit order depends on spectral order
    #use type=fit to write out the continuum fit
    cont_sample = '"'+cont[j-1].replace('\n','')+'"'
    if j !=2 and j !=3:
        iraf.continuum(input='ftell_nolines'+str(j),output='fit'+str(j),ask='yes',lines='*',bands='1',type="fit",replace='no',wavescale='yes',logscale='no',override='no',listonly='no',logfiles='',inter=continuuminter,sample=cont_sample,naverage=1,func='spline3',order=5,low_rej=1.0,high_rej=3.0,niterate=2,grow=1.0,markrej='yes',graphics='stdgraph',cursor='',mode='ql')
    elif j == 2:
        iraf.continuum(input='ftell_nolines'+str(j),output='fit'+str(j),ask='yes',lines='*',bands='1',type="fit",replace='no',wavescale='yes',logscale='no',override='no',listonly='no',logfiles='',inter=continuuminter,sample=cont_sample,naverage=1,func='spline3',order=2,low_rej=1.0,high_rej=3.0,niterate=2,grow=1.0,markrej='yes',graphics='stdgraph',cursor='',mode='ql')
    elif j == 3:
        iraf.continuum(input='ftell_nolines'+str(j),output='fit'+str(j),ask='yes',lines='*',bands='1',type="fit",replace='no',wavescale='yes',logscale='no',override='no',listonly='no',logfiles='',inter=continuuminter,sample=cont_sample,naverage=1,func='spline3',order=3,low_rej=1.0,high_rej=3.0,niterate=2,grow=1.0,markrej='yes',graphics='stdgraph',cursor='',mode='ql')
    #rather than fitting the continuum again with type=ratio, let's just divide the spectrum by the fit
    iraf.imarith ('ftell_nolines'+str(j), "/", 'fit'+str(j), result='norm'+str(j),title='',divzero=0.0,hparams='',pixtype='',calctype='',verbose='no',noact='no',mode='al')


    #Use "telluric" to divide telluric lines out of science target, then remove normalisation again
    #Seem to get better results from telluric if use *continuum-divided* standard spectrum (?)
    #Start with "standard" extraction
    print( "")
    print( "***Removing telluric lines from galaxy spectrum, extension", str(j) )
    print( "")
    tell_sample2 = '"'+tell2[j-1].replace('\n','')+'"'
    tell_info = iraf.telluric(input='v'+target+'[sci,'+str(j)+']',output='vtella'+str(j),cal='norm'+str(j),airmass=airmass_target,answer='yes',ignoreaps='yes',xcorr='yes',tweakrms='yes',inter=telluricinter,sample=tell_sample2,threshold=0.1,lag=3,shift=0.0,dshift=0.1,scale=1.0,dscale=0.1,offset=1,smooth=1,cursor='',mode='al',Stdout=1)

    #record shift & scale info for future reference, write message in Checks_and_Warnings file
    t2.write(str(tell_info)+'\n')
    if "limits" in tell_info[-1].split()[-1]:
        index = -2
    else:
        index = -1
    norm = tell_info[index].split()[-1]
    scale = tell_info[index].split()[-4].replace(',','')
    shift = tell_info[index].split()[-7].replace(',','')
    if abs(float(shift) > 1.0):
        message = '***WARNING:'
        info = " (shift > 1 pix)"
    else:
        message = "***CHECK:"
        info = " (shift < 1 pix)"
    print( message, "Telluric task found shift = ", shift, "pix in extension ", str(j), info)
    if float(scale) > 1.1 or float(scale) < 0.9:
        message = '***WARNING:'
        info = " (scale < 0.9 or scale > 1.1)"
    else:
        message = "***CHECK:"
        info = " (0.9 < scale < 1.1)"
    print( message, "Telluric task found scale = ", scale, " in extension ", str(j), info)
    
    #re-introduce continuum shape of standard star (after running telluric using continuum-normalised version)
    #also take out "normalisation" factor introduced by telluric

    #"standard" extraction
    iraf.imarith (operand1='vtella'+str(j), op="/", operand2=norm, result='vtellb'+str(j),title='',divzero=0.0,hparams='',pixtype='',calctype='',verbose='no',noact='no',mode='al')
    iraf.imarith (operand1='vtellb'+str(j), op="/", operand2='fit'+str(j), result='fvtell'+str(j),title='',divzero=0.0,hparams='',pixtype='',calctype='',verbose='no',noact='no',mode='al')

    if extras == "yes":
        #full-slit extraction
        #take shift, scale etc. from last call to "telluric" and use in call to telluric with full-slit extraction
        iraf.telluric(input='a'+target+'[sci,'+str(j)+']',output='atella'+str(j),cal='norm'+str(j),sample=tell_sample2,airmass=airmass_target,answer='yes',ignoreaps='yes',xcorr='no',tweakrms='no',inter="no",threshold=0.1,lag=3,shift=shift,dshift=0.0,scale=scale,dscale=0.0,offset=1,smooth=1,cursor='',mode='al')
        iraf.imarith (operand1='atella'+str(j), op="/", operand2=norm, result='atellb'+str(j),title='',divzero=0.0,hparams='',pixtype='',calctype='',verbose='no',noact='no',mode='al')
        iraf.imarith (operand1='atellb'+str(j), op="/", operand2='fit'+str(j), result='fatell'+str(j),title='',divzero=0.0,hparams='',pixtype='',calctype='',verbose='no',noact='no',mode='al')
        #the step-by-step extraction
        for k in range (1, steps): 
            #take shift, scale etc. from next-to-last call to "telluric" and use in call to telluric with stepwise extractions
            iraf.telluric(input='s'+str(k)+''+target+'[sci,'+str(j)+']',output='s'+str(k)+'tella'+str(j),cal='norm'+str(j),sample=tell_sample2,airmass=airmass_target,answer='yes',ignoreaps='yes',xcorr='no',tweakrms='no',inter="no",threshold=0.1,lag=3,shift=shift,dshift=0.0,scale=scale,dscale=0.0,offset=1,smooth=1,cursor='',mode='al')
            iraf.imarith (operand1='s'+str(k)+'tella'+str(j), op="/", operand2=norm, result='s'+str(k)+'tellb'+str(j),title='',divzero=0.0,hparams='',pixtype='',calctype='',verbose='no',noact='no',mode='al')
            iraf.imarith (operand1='s'+str(k)+'tellb'+str(j), op="/", operand2='fit'+str(j), result='fs'+str(k)+'tell'+str(j),title='',divzero=0.0,hparams='',pixtype='',calctype='',verbose='no',noact='no',mode='al')


file.close()
t2.close()
