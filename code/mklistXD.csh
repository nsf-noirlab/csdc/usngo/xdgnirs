#!/bin/csh
#
##
#####  Makes the lists of files needed by XDpiped.csh
##  Original version: O. Gonzalez Martin (February 2012).
##  Extensive updates by REM

set infilen = $1
set startdir = $2
set procdir = $3

#The overall list of files comes from ObsIdentifyXD.py
#This code sorts them into the lists needed by the pipeline, based on the header keywords

echo "List of science target files (target.lst):" | tee -a ${startdir}/LISTS/Status_${infilen}.txt 
echo "########################################"
cat ${startdir}/LISTS/ID${infilen}.lst | grep OBJECT | grep science  | awk '{print $1}' | tee -a ${startdir}/LISTS/Status_${infilen}.txt 
cat ${startdir}/LISTS/ID${infilen}.lst | grep OBJECT | grep science  | awk '{print $1}' >! ${startdir}/LISTS/target.lst

#Check that we have one and only one science target
set target_name = `cat ${startdir}/LISTS/ID${infilen}.lst | grep -- OBJECT | grep science  | head -2 | tail -1 | awk '{print $2}'`
echo $target_name >! ${startdir}/LISTS/sci_name.txt
if (! -e ${startdir}/LISTS/target.lst || -z ${startdir}/LISTS/target.lst) then
    echo "ERROR no science target spectra detected" | tee -a ${startdir}/LISTS/Status_${infilen}.txt
    exit
endif
cat ${startdir}/LISTS/ID${infilen}.lst | grep OBJECT | grep science  | awk '{print $2}' >! ${startdir}/LISTS/object.lst
set intruder = `grep -v $target_name ${startdir}/LISTS/object.lst`
if (${intruder} != "") then
    echo "***WARNING: You seem to be trying to reduce two different science targets at the same time. This seems like a bad thing. XDGNIRS will try it, but would prefer you to stop and check your input files." | tee -a ${startdir}/LISTS/Status_${infilen}.txt
endif
rm ${startdir}/LISTS/object.lst >& /dev/null

#Sort the spectra into A and B beams
#We simply look for spectra with identical Q offsets, and we look for two of them
#This is why XDGNIRS can currently only handle ABBA, AB, or (probably) ABA patterns with nods in the Q direction
#And it won't recognise data taken after a reacquisition, because the Q offsets in the headers have changed
set firstoffsetP = `cat ${startdir}/LISTS/ID${infilen}.lst | grep OBJECT | grep science  | head -1 | awk '{print $13}' `
set firstoffsetQ = `cat ${startdir}/LISTS/ID${infilen}.lst | grep OBJECT | grep science  | head -1 | awk '{print $14}' `
set sndoffsetP = `cat ${startdir}/LISTS/ID${infilen}.lst | grep OBJECT | grep science  | head -2 | tail -1 | awk '{print $13}' `
set sndoffsetQ = `cat ${startdir}/LISTS/ID${infilen}.lst | grep OBJECT | grep science  | head -2 | tail -1 | awk '{print $14}' `

echo "List of science target A nod files (targetA.lst):" | tee -a ${startdir}/LISTS/Status_${infilen}.txt 
echo "########################################"
cat ${startdir}/LISTS/ID${infilen}.lst | grep OBJECT | grep science  | grep -w -- ${firstoffsetQ}  | awk '{print $1}' | tee -a ${startdir}/LISTS/Status_${infilen}.txt 
cat ${startdir}/LISTS/ID${infilen}.lst | grep OBJECT | grep science  | grep -w -- ${firstoffsetQ}  | awk '{print $1}' >! ${startdir}/LISTS/targetA.lst
if (! -e ${startdir}/LISTS/targetA.lst || -z ${startdir}/LISTS/targetA.lst) then
    echo "ERROR no science target 'A' beam spectra detected" | tee -a ${startdir}/LISTS/Status_${infilen}.txt
    exit
endif

echo "List of science target B nod files (targetB.lst):" | tee -a ${startdir}/LISTS/Status_${infilen}.txt 
echo "########################################"
cat ${startdir}/LISTS/ID${infilen}.lst | grep OBJECT | grep science  | grep -w -- ${sndoffsetP} | grep -w -- ${sndoffsetQ}  | awk '{print $1}' | tee -a ${startdir}/LISTS/Status_${infilen}.txt 
cat ${startdir}/LISTS/ID${infilen}.lst | grep OBJECT | grep science  | grep -w -- ${sndoffsetP} | grep -w -- ${sndoffsetQ}  | awk '{print $1}' >! ${startdir}/LISTS/targetB.lst

if (! -e ${startdir}/LISTS/targetB.lst || -z ${startdir}/LISTS/targetB.lst) then
    echo "ERROR no science target 'B' beam spectra detected" | tee -a ${startdir}/LISTS/Status_${infilen}.txt
    exit
endif

echo "IRhigh flats (IRhigh.lst):" | tee -a ${startdir}/LISTS/Status_${infilen}.txt 
echo "########################################"
cat ${startdir}/LISTS/ID${infilen}.lst | grep FLAT | grep arcsec | grep IRhigh | awk '{print $1}' | tee -a ${startdir}/LISTS/Status_${infilen}.txt 
cat ${startdir}/LISTS/ID${infilen}.lst | grep FLAT | grep arcsec | grep IRhigh | awk '{print $1}' >! ${startdir}/LISTS/IRhigh.lst
if (! -e ${startdir}/LISTS/IRhigh.lst || -z ${startdir}/LISTS/IRhigh.lst.lst) then
    echo "ERROR no IR flats detected" | tee -a ${startdir}/LISTS/Status_${infilen}.txt
    exit
endif

echo "QH flats (QH.lst):" | tee -a ${startdir}/LISTS/Status_${infilen}.txt 
echo "########################################"
cat ${startdir}/LISTS/ID${infilen}.lst | grep FLAT | grep arcsec | grep QH | awk '{print $1}' | tee -a ${startdir}/LISTS/Status_${infilen}.txt 
cat ${startdir}/LISTS/ID${infilen}.lst | grep FLAT | grep arcsec | grep QH | awk '{print $1}' >! ${startdir}/LISTS/QH.lst
if (! -e ${startdir}/LISTS/QH.lst || -z ${startdir}/LISTS/QH.lst) then
    echo "ERROR no QH flats detected" | tee -a ${startdir}/LISTS/Status_${infilen}.txt
    exit
endif

echo "Arcs (Arc.lst):" | tee -a ${startdir}/LISTS/Status_${infilen}.txt 
echo "########################################"
cat ${startdir}/LISTS/ID${infilen}.lst | grep ARC | awk '{print $1}' | tee -a ${startdir}/LISTS/Status_${infilen}.txt 
cat ${startdir}/LISTS/ID${infilen}.lst | grep ARC | awk '{print $1}' >! ${startdir}/LISTS/Arc.lst
if (! -e ${startdir}/LISTS/Arc.lst || -z ${startdir}/LISTS/Arc.lst) then
    echo "ERROR no arc spectra detected" | tee -a ${startdir}/LISTS/Status_${infilen}.txt
    exit
endif

echo "List of standard files (standard.lst):" | tee -a ${startdir}/LISTS/Status_${infilen}.txt 
echo "########################################"
cat ${startdir}/LISTS/ID${infilen}.lst | grep OBJECT | grep partnerCal  | awk '{print $1}' | tee -a ${startdir}/LISTS/Status_${infilen}.txt 
cat ${startdir}/LISTS/ID${infilen}.lst | grep OBJECT | grep partnerCal | awk '{print $1}' >! ${startdir}/LISTS/standard.lst
set standard_name = `cat ${startdir}/LISTS/ID${infilen}.lst | grep OBJECT | grep partnerCal  | head -2 | tail -1 | awk '{print $2}'`
echo $standard_name >! ${startdir}/LISTS/std_name.txt
if (! -e ${startdir}/LISTS/standard.lst || -z ${startdir}/LISTS/standard.lst) then
    echo "ERROR no standard star spectra detected" | tee -a ${startdir}/LISTS/Status_${infilen}.txt
    exit
endif
cat ${startdir}/LISTS/ID${infilen}.lst | grep OBJECT | grep partnerCal | awk '{print $2}' >! ${startdir}/LISTS/telluric.lst
set intruder = `grep -v $standard_name ${startdir}/LISTS/telluric.lst`
if (${intruder} != "") then
    echo "***WARNING: It looks as if you've given two different telluric standards to XDGNIRS. The code doesn't know how to handle that; it will try anyway, but would prefer you to stop and check your input files." | tee -a ${startdir}/LISTS/Status_${infilen}.txt
endif
rm ${startdir}/LISTS/telluric.lst >& /dev/null

set firstoffsetP = `cat ${startdir}/LISTS/ID${infilen}.lst | grep OBJECT | grep partnerCal  | head -1 | awk '{print $13}'`
set firstoffsetQ = `cat ${startdir}/LISTS/ID${infilen}.lst | grep OBJECT | grep partnerCal  | head -1 | awk '{print $14}'`
set sndoffsetP = `cat ${startdir}/LISTS/ID${infilen}.lst | grep OBJECT | grep partnerCal  | head -2 | tail -1 | awk '{print $13}'`
set sndoffsetQ = `cat ${startdir}/LISTS/ID${infilen}.lst | grep OBJECT | grep partnerCal  | head -2 | tail -1 | awk '{print $14}'`

#This was something to do with spectra starting in the B position being confusing to the code...
#I thought I'd put this functionality in FindSpectra.py, so why is it still here?
set abs1 = `echo "$firstoffsetQ" | awk '{if ($1 < 0) $1 = -$1; print}'`
set abs2 = `echo "$sndoffsetQ" | awk '{if ($1 < 0) $1 = -$1; print}'`
set swap = `echo "$abs1 $abs2" | awk '{if ($1 > $2) print "swap"; else print "noswap"}'`
if  ($swap == "swap") then
    set temp = ${sndoffsetQ}
    set sndoffsetQ = ${firstoffsetQ}
    set firstoffsetQ = ${temp}
endif


echo "List of standard A nod files (standardA.lst):" | tee -a ${startdir}/LISTS/Status_${infilen}.txt 
echo "########################################"
cat ${startdir}/LISTS/ID${infilen}.lst | grep OBJECT | grep partnerCal  | grep -w -- ${firstoffsetQ}  | awk '{print $1}' | tee -a ${startdir}/LISTS/Status_${infilen}.txt 
cat ${startdir}/LISTS/ID${infilen}.lst | grep OBJECT | grep partnerCal  | grep -w -- ${firstoffsetQ}  | awk '{print $1}' >! ${startdir}/LISTS/standardA.lst
if (! -e ${startdir}/LISTS/standardA.lst || -z ${startdir}/LISTS/standardA.lst) then
    echo "ERROR no standard star 'A' beam spectra detected" | tee -a ${startdir}/LISTS/Status_${infilen}.txt
    exit
endif

echo "List of standard B nod files (standardB.lst):" | tee -a ${startdir}/LISTS/Status_${infilen}.txt 
echo "########################################"
cat ${startdir}/LISTS/ID${infilen}.lst | grep OBJECT | grep partnerCal  | grep -w -- ${sndoffsetQ}  | awk '{print $1}' | tee -a ${startdir}/LISTS/Status_${infilen}.txt 
cat ${startdir}/LISTS/ID${infilen}.lst | grep OBJECT | grep partnerCal  | grep -w -- ${sndoffsetQ}  | awk '{print $1}' >! ${startdir}/LISTS/standardB.lst
if (! -e ${startdir}/LISTS/standardB.lst || -z ${startdir}/LISTS/standardB.lst) then
    echo "ERROR no standard star 'B' beam spectra detected" | tee -a ${startdir}/LISTS/Status_${infilen}.txt
    exit
endif

echo "Pinhole flats (pinholes.lst):" | tee -a ${startdir}/LISTS/Status_${infilen}.txt 
echo "########################################"
cat ${startdir}/LISTS/ID${infilen}.lst | grep FLAT | grep Pinholes   | awk '{print $1}' | head -1 | tail -1 | tee -a ${startdir}/LISTS/Status_${infilen}.txt 
cat ${startdir}/LISTS/ID${infilen}.lst | grep FLAT | grep Pinholes   | awk '{print $1}' | head -1 | tail -1 >! ${startdir}/LISTS/pinholes.lst
if (! -e ${startdir}/LISTS/pinholes.lst || -z ${startdir}/LISTS/pinholes.lst) then
    echo "ERROR no pinhole spectra detected" | tee -a ${startdir}/LISTS/Status_${infilen}.txt
    exit
endif

#Now we find the acquisition images for the standard star and science target
#They are needed because we later need to find out where the spectra are on the slit, which depends on the P/Q offsets
#Unfortunately the P/Q offsets in the OT aren't written into the headers; have to subtract off the telescope position at the end of the acquisition...
#See FindSpectra/py for more information
echo "List of acquisition images (acq.lst):" | tee -a ${startdir}/LISTS/Status_${infilen}.txt 
echo "########################################"
cat ${startdir}/LISTS/ID${infilen}.lst | grep OBJECT | grep acq | awk '{print $1" "$2}'  | tee -a ${startdir}/LISTS/Status_${infilen}.txt 
cat ${startdir}/LISTS/ID${infilen}.lst | grep OBJECT | grep acq | awk '{print $1" "$2}' >! ${startdir}/LISTS/acq.lst
if (! -e ${startdir}/LISTS/acq.lst || -z ${startdir}/LISTS/acq.lst) then
    echo "ERROR no acquisition images detected" | tee -a ${startdir}/LISTS/Status_${infilen}.txt
    exit
endif

echo "List of science target acq images (acqtarget.lst):" | tee -a ${startdir}/LISTS/Status_${infilen}.txt 
echo "########################################"
set target_name = `cat ${startdir}/LISTS/sci_name.txt`
cat ${startdir}/LISTS/acq.lst | grep -- ${target_name} | awk '{print $1}' | tee -a ${startdir}/LISTS/Status_${infilen}.txt 
cat ${startdir}/LISTS/acq.lst | grep -- ${target_name} | awk '{print $1}' >! ${startdir}/LISTS/acqtarget.lst
if (! -e ${startdir}/LISTS/acqtarget.lst || -z ${startdir}/LISTS/acqtarget.lst) then
    echo "ERROR no science target acquisition images detected" | tee -a ${startdir}/LISTS/Status_${infilen}.txt
    exit
endif

echo "List of standard star acq images (acqstd.lst):" | tee -a ${startdir}/LISTS/Status_${infilen}.txt 
echo "########################################"
set standard_name = `cat ${startdir}/LISTS/std_name.txt`
cat ${startdir}/LISTS/acq.lst | grep -- ${standard_name} | awk '{print $1}' | tee -a ${startdir}/LISTS/Status_${infilen}.txt 
cat ${startdir}/LISTS/acq.lst | grep -- ${standard_name} | awk '{print $1}' >! ${startdir}/LISTS/acqstd.lst
if (! -e ${startdir}/LISTS/acqstd.lst || -z ${startdir}/LISTS/acqstd.lst) then
    echo "ERROR no standard star acquisition images detected" | tee -a ${startdir}/LISTS/Status_${infilen}.txt
    exit
endif

#Changing the order of the concatenation of these files will affect the meaning of the stats calculated by StatsAndPrepare.py
#So don't do it...
cat ${startdir}/LISTS/QH.lst ${startdir}/LISTS/IRhigh.lst ${startdir}/LISTS/Arc.lst >! ${startdir}/LISTS/NightCal.lst
cat ${startdir}/LISTS/NightCal.lst ${startdir}/LISTS/standard.lst ${startdir}/LISTS/target.lst >! ${startdir}/LISTS/NightObs.lst
cat ${startdir}/LISTS/QH.lst ${startdir}/LISTS/IRhigh.lst ${startdir}/LISTS/pinholes.lst >! ${startdir}/LISTS/Cals.lst
cat ${startdir}/LISTS/standard.lst ${startdir}/LISTS/target.lst ${startdir}/LISTS/IRhigh.lst ${startdir}/LISTS/QH.lst ${startdir}/LISTS/Arc.lst ${startdir}/LISTS/pinholes.lst >! ${startdir}/LISTS/All.lst
if (! -e ${startdir}/LISTS/All.lst || -z ${startdir}/LISTS/All.lst) then
    echo "ERROR no master data list detected" | tee -a ${startdir}/LISTS/Status_${infilen}.txt
    exit
endif

