\contentsline {section}{\numberline {1}Introduction}{3}
\contentsline {section}{\numberline {2}Code, Access and Requirements}{5}
\contentsline {subsection}{\numberline {2.1}The Code}{5}
\contentsline {subsection}{\numberline {2.2}The Data}{6}
\contentsline {section}{\numberline {3}Inspecting Your Input Data}{7}
\contentsline {section}{\numberline {4}Running the Pipeline (aka ``if you don't read anything else'')}{7}
\contentsline {section}{\numberline {5}The Reduction Steps}{9}
\contentsline {subsection}{\numberline {5.1}Identifying the Input Files (Step=0)}{10}
\contentsline {subsection}{\numberline {5.2}Pattern Noise Cleaning (Step=1)}{11}
\contentsline {subsubsection}{\numberline {5.2.1}Example: NGC 3031}{12}
\contentsline {subsection}{\numberline {5.3}Preparing the Data (Step=2)}{12}
\contentsline {subsubsection}{\numberline {5.3.1}Example: NGC 3031}{14}
\contentsline {subsection}{\numberline {5.4}Making the Flatfield (Step=3)}{16}
\contentsline {subsubsection}{\numberline {5.4.1}Example: NGC 3031}{17}
\contentsline {subsection}{\numberline {5.5}Reducing the Science Target and Standard Star Data (Step=4)}{17}
\contentsline {subsubsection}{\numberline {5.5.1}Example: NGC 3031}{20}
\contentsline {subsection}{\numberline {5.6}S-distortion Correction and Wavelength Calibration (Step=5)}{21}
\contentsline {subsubsection}{\numberline {5.6.1}Example: NGC 3031}{23}
\contentsline {subsection}{\numberline {5.7}Combining the Files (Step=6)}{24}
\contentsline {subsubsection}{\numberline {5.7.1}Example: NGC 3031}{25}
\contentsline {subsection}{\numberline {5.8}Extracting the Spectra (Step=7)}{26}
\contentsline {subsubsection}{\numberline {5.8.1}Example: NGC 3031}{28}
\contentsline {subsection}{\numberline {5.9}H Line Removal (Step=8)}{28}
\contentsline {subsubsection}{\numberline {5.9.1}Example: NGC 3031}{30}
\contentsline {subsection}{\numberline {5.10}Telluric Line Cancellation (Step=9)}{30}
\contentsline {subsubsection}{\numberline {5.10.1}Example: NGC 3031}{32}
\contentsline {subsection}{\numberline {5.11}Flux Calibration (Step=10)}{32}
\contentsline {subsubsection}{\numberline {5.11.1}Example: NGC 3031}{34}
\contentsline {subsection}{\numberline {5.12}Combining the Orders (Step=11)}{34}
\contentsline {subsubsection}{\numberline {5.12.1}Example: NGC 3031}{36}
\contentsline {subsection}{\numberline {5.13}Generating an Error Spectrum (Step=12)}{36}
\contentsline {subsection}{\numberline {5.14}Science Target Data Sheets (Step=13)}{36}
\contentsline {subsubsection}{\numberline {5.14.1}Example: NGC 3031}{37}
\contentsline {section}{\numberline {6}What to look for}{38}
\contentsline {section}{\numberline {7}Usage/Syntax Examples}{39}
\contentsline {section}{\numberline {8}Troubleshooting, Support, and Acknowledgment}{40}
\contentsline {section}{\numberline {9}History}{40}
\contentsline {section}{\numberline {10}Wishlist}{40}
