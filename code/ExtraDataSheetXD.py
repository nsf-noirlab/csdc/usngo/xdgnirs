#!/usr/bin/env python
#
##
##	WriteDataSheetXD.py:   		
##
##
#
# 2015.09.22: (jmiller,mpohlen) import matplotlib to avoid (in cron)
#             'tkinter.TclError: no display name and no $DISPLAY' error    
#
# Import libraries
#
import sys
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.table import table
from pylab import *
import numpy as np

from scipy.stats import median_abs_deviation

### Inputs
spectrum = str(sys.argv[2:3]).replace("['","").replace("']","")+"_rest.txt"
wav_key = "Rest"
try:
   with open(spectrum) as f: pass
except IOError as e:
    print( "Warning: no redshift-corrected spectrum available for data sheet; using observed frame spectrum")
    wav_key = "Observed"
    spectrum = str(sys.argv[2:3]).replace("['","").replace("']","")+".txt"
    try:
        with open(spectrum) as f: pass
    except IOError as e2:
        print( "ERROR: Can't find reduced spectrum, "+spectrum+", for making data sheet. Exiting script")
        print( "")
        sys.exit()

vegafile = sys.argv[1:2][0]
version = str(sys.argv[3:5]).replace("[","").replace("]","").replace("'","")
date = str(sys.argv[6:11]).replace("[","").replace("]","").replace("'","").replace(",","")
redshift = float(sys.argv[11:12][0])

#Latitude of GN (for parallactic angle calc)
#19:49:25 deg
latitude = 19. + 49./60. + 25./3600.
latitude = radians(latitude)

absunits = sys.argv[12:13][0]
if absunits == "yes":
   y_label = '$10^{-15} erg \; cm^{-2} \; s^{-1} \; \AA^{-1}$'
else:
   y_label = "F$_{\lambda}$, arbitrary units"


#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#MAKING TABLE OF VALUES FROM PIPELINE
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


#Read in the PEAK COUNTS/FRAME AND FWHM

def read_measurements (file):
    peak = []
    fwhm = []
    i = 0
    info = open (file)
    for line in info:
        spaces = 'yes'
        #imexam sometimes puts spaces after "=", which messes things up...
        while spaces == 'yes':
            if "= " in line:
                line = line.replace("= ", "=")
            elif "= " not in line:
                spaces= 'no'
        strings = list(map(str, line.split()))
        #ignore odd-numbered lines, they just contain filename etc.
        i = i + 1
        if not i%2:
           peak.append(float(strings[3][5:]))
           #convert fwhm from pix to arcsec
           fwhm.append(float(strings[5][5:])* 0.15)
    #Find counts, return only bit before decimal point
    peak = int(round(peak[0],0))
    #Find FWHM
    fwhm = round(fwhm[0],2)
    info.close()
    return peak, fwhm


#Read in the HEADER INFO
#order of entries:
#hselect targetson_comb[0] field=OBJECT,GEMPRGID,AIRMASS,HA,PA,RAWIQ,RAWCC,RAWWV,RAWBG,AZIMUTH,DEC,DATE-OBS expr=yes  > target_info.txt

def read_info (file):
    info = open (file)
    for line in info:
        strings = list(map(str, line.split()))
        object = strings[0]
        i = 0
        id = strings[1+i]
        airmass = strings[2+i]
        ha = strings[3+i]
        pa = strings[4+i].split(".")[0]
        iq = strings[5+i].replace("-percentile", "")
        cc = strings[6+i].replace("-percentile", "")
        wv = strings[7+i].replace("-percentile", "")
        bg = strings[8+i].replace("-percentile", "")
        az = float(strings[9+i])
        dec = float(strings[10+i])
        obsdate = strings[11+i]
    return object, id, airmass, ha, pa, iq, cc, wv, bg, az, dec, obsdate
    info.close()


#Read in the SNR
def read_snr (file):
    info = open (file)
    for line in info:
        strings = list(map(str, line.split()))
        sn = strings[6].split(".")[0]
    return sn


#Calculate the PARALLACTIC ANGLE
#This and slit PA come from start of observation; mean parallactic angle will be a bit different
def parallactic_angle (azimuth, declination, slit_pa):
    azimuth = radians(azimuth)
    declination = radians(declination)
    parallactic_angle = sin(azimuth) * cos(latitude)/cos(declination)
    parallactic_angle = round(parallactic_angle,2)
    parallactic_angle = math.asin(parallactic_angle)
    parallactic_angle = degrees(parallactic_angle)
    #find the difference between the parallactic angle and the slit PA
    dev = abs(parallactic_angle - float(slit_pa))
    #return just the bit before the decimal point
    parallactic_angle = str(parallactic_angle).split(".")[0]
    dev = str(dev).split(".")[0]
    return parallactic_angle, dev

#Do all the stuff
std_peak, std_fwhm = read_measurements ("standard.log")
gal_peak, gal_fwhm = read_measurements ("target.log")

galaxy, prgid, gal_airmass, gal_ha, gal_pa, gal_iq, gal_cc, gal_wv, gal_bg, gal_az, gal_dec, gal_obsdate = read_info ("target_info.txt")
standard, prgid, std_airmass, std_ha, std_pa, std_iq, std_cc, std_wv, std_bg, std_az, std_dec, std_obsdate  = read_info ("standard_info.txt")

gal_sn = read_snr("snrtgt.txt")
std_sn = read_snr("snrstd.txt")

gal_angle, gal_diff = parallactic_angle(gal_az, gal_dec, gal_pa)
std_angle, std_diff = parallactic_angle(std_az, std_dec, std_pa)
#gal_angle = gal_diff =  'N/A'
#std_angle = std_diff =  'N/A'

#Make tables
fig = plt.figure ()
ax = fig.add_subplot(211, frame_on=False)
ax.xaxis.set_visible(False)
ax.yaxis.set_visible(False)


def format_table(location, jrange, cellsize):
    table1 = table(cellText=cellText, colLabels=colLabels, loc=location)
    table1.auto_set_font_size(False)
    table1.set_fontsize(7)
    cellDict = table1.get_celld()
    for i in (0,1,2):
        for j in (jrange):
            cellDict[(i,j)].set_width(cellsize)
        cellDict[(i,0)].set_width(0.25)

#Have to split into two
#First part
colLabels = (prgid[5:]+", "+gal_obsdate, 'Total counts, K', 'FWHM ("), K', "S/N, 2.1-2.2 um", 'Airmass', 'HA')
cellText = [[galaxy, gal_peak, gal_fwhm, gal_sn, gal_airmass[0:4], gal_ha[0:6]], [standard, std_peak, std_fwhm, std_sn, std_airmass[0:4], std_ha[0:6]]]
format_table('upper center', (4,5), 0.1)

#Second part:
colLabels = (prgid[5:]+", "+std_obsdate, 'Slit, Par. Ang', "Diff", 'IQ', 'CC', 'WV', 'SB')
cellText = [[galaxy,  gal_pa+r"$^{\circ}$, "+str(gal_angle)[0:4]+r"$^{\circ}$", gal_diff+r"$^{\circ}$" ,gal_iq, gal_cc, gal_wv, gal_bg], [standard, std_pa+r"$^{\circ}$, "+str(std_angle)[0:4]+r"$^{\circ}$", std_diff+r"$^{\circ}$" ,std_iq, std_cc, std_wv, std_bg]]
format_table('center', (2,3,4,5,6), 0.111)

ax.text (0.0, 0.28, version+",  "+date, size=7)

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#PLOTTING THE FINAL SPECTRUM
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#function to read in spectral data from file
#doesn't really need to be a function; is taken from other code...
def readspectra (file):
    wav = []
    flux = []
    data = open (file)
    for line in data:
        strings = list(map(str, line.split()))
        #read observed wavelength and convert to microns
        wav.append(float(strings[0]) * 0.0001)
        flux.append(float(strings[1]))
    return wav,flux


def plotVega (ymax):
#vega spectrum, obtained by doing this
#odcombine vega_ext[1],vega_ext[2],vega_ext[3],vega_ext[4],vega_ext[5],vega_ext[6] group=all output=vega_all
#wspectext vega_all output=vega_all.txt header=no
    vega_wav = []
    vega_trans = []
    vega = open (vegafile)
    a = -1
    for line in vega:
        a = a + 1
        strings = list(map(str, line.split()))
        #convert to microns
        vega_wav.append(float(strings[0])* 0.0001)
        vega_trans.append(float(strings[1]))
        #shift by same amount as galaxy, if redshift-corrected galaxy spectrum being used
        if wav_key == "Rest":
           vega_wav[a] = vega_wav[a] / (1 + redshift)
    #scale to fit y axes
    scalefac = ymax / max(vega_trans)
    for i in range (0, len(vega_trans)):
        vega_trans[i] = vega_trans[i] * scalefac
    #vega_trans = vega_trans * (ymax / max(vega_trans))
    plt.plot (vega_wav, vega_trans, "b--")
    vega.close()

def setup (f, ax, plot_h_lines):
    #plotting stuff
    ymax = np.median(f)*2.5
    xmax = 2.5/(1+redshift)

    if plot_h_lines == "yes":
        #plot the Vega spectrum to show possible H line residuals
        plotVega (ymax)

    ax.plot (mic, f, 'k-')
    ax.text (0.85, 0.85, galaxy, transform=ax.transAxes, size=8)
    plt.setp (ax.get_xticklabels (), fontsize=8)
    plt.setp (ax.get_yticklabels (), fontsize=8)
    ax.axis([0.83/(1+redshift), xmax, 0, ymax])


#Open the file and make the figure
file = spectrum

mic,flam = readspectra (file)
ax = fig.add_subplot(212)
for i in range (0, len(flam)):
#put y axis on sensible scale
    flam[i] = flam[i] * 1e15
setup (flam, ax, "yes")
ax.set_ylabel (r''+y_label, size=8)
ax.set_xlabel (r''+wav_key+" wavelength, $\mu$m", size=8)

#max_val = np.average(flam) + (1 * np.std(flam))
max_val = 2 * np.average(flam)
min_val = np.average(flam) - (2 * np.std(flam))

if min_val <= 0:
   min_val=0

#ax.set_ylim(min_val,max_val)
plt.ylim(min_val,max_val)

plt.savefig('../PRODUCTS/'+galaxy+'_data_sheet.pdf')

print('LOOK HERE')
print(max_val)
print(min_val)
print()
print('std = ',np.std(flam))
print('average = ',np.average(flam))
